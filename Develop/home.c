#include <revolution.h>
#include <revolution/kpad.h>
#define DEMO_USE_MEMLIB
#include <demo.h>			//
#include <revolution/gx.h> 	// OSFatalを使用する場合includeが必要

#include "mem2allocator.h"
#include <math.h>
#include <stdio.h>			// sprintfなども使用できます
#include <stdlib.h>
#include "main.h"
#include "sprite.h"
#include "Controller.h"
#include "home.h"

static TPLPalettePtr texPal;				//テクスチャパレットを表示する型の変数
static GXTexObj HomeTexObj[TEXTURE_HOME_MAX];	//テクスチャオブジェクトを保存する配列
static int g_nID;
static int g_SeneID;
static int g_flame;
static bool g_fade;

void HomeCase(void)
{
	HomeUpdate();
	HomeDraw();
}
void HomeInit(void)
{
	int nCount;

	// TPLファイルノロード
	TPLGetPalette(&texPal, "home.tpl");

	//ロードエラーチェック
	ASSERTMSG(TEXTURE_HOME_MAX == texPal->numDescriptors, "\nTPL内画像総数とNUM_MAXの値が一致していない\n(numDescriptors==%d)!=(NUM_MAX=%d)\n", texPal->numDescriptors, TEXTURE_HOME_MAX);

	//TPLからテクスチャオブジェクトを取り出す
	for (nCount = 0; nCount < texPal->numDescriptors; nCount++)
	{
		TPLGetGXTexObjFromPalette(texPal, &HomeTexObj[nCount], nCount);
	}
	HomeInit2();
}
void HomeInit2(void)
{
	g_nID = 0;
	g_flame = 0;
	g_fade = false;
	g_SeneID = 0;
}
void HomeUninit(void)
{
	// テクスチャパレットを解放
	TPLReleasePalette(&texPal);
}
void HomeUpdate(void)
{
	if (g_SeneID != 0)
	{
		g_flame++;
		g_fade = true;
	}
}
void HomeDraw(void)
{
	switch (g_SeneID)
	{
	case 0:
		Home1();
		break;
	case 1:
		HomeMan();
		break;
	case 2:
		HomeGale();
		break;
	default:
		break;
	}
}

int HomeGetFade(void)
{
	return g_fade;
}

void Home1(void)
{
	u16 tw = 0;	//横サイズ
	u16 th = 0;	//縦サイズ
	int PosY = 0;
	int PosX = 0;

	SpriteSetup();
	SpriteSetColor(255, 255, 255, 255);
	SpritrSetScale(1.0f, 1.0f);
	TexSetSize(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	SpritrSetSize(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	SpriteDraw(&HomeTexObj[TEXTURE_NUM_HOME]);

	tw = GXGetTexObjWidth(&HomeTexObj[TEXTURE_FontMan]);	//横サイズ
	th = GXGetTexObjHeight(&HomeTexObj[TEXTURE_FontMan]);	//縦サイズ
	SpriteSetup();
	SpritrSetScale(1.0f, 1.0f);
	TexSetSize(0, 0, tw, th);
	th = th / 2;
	tw = tw / 2;
	PosY = 250;
	SpritrSetSize(PosX, PosY, tw, th);
	if (PosX <= GetPX() && GetPX() <= (PosX + tw))
	{
		if (PosY <= GetPY() && GetPY() <= (PosY + th))
		{
			SpriteSetColor(242, 255, 0, 255);
			g_nID = 1; // 男
			if (FadeFlag() == true)
			{
				g_SeneID = g_nID;
			}
		}
	}
	else
	{
		SpriteSetColor(255, 255, 255, 255);
		g_nID = 0;
	}
	SpriteDraw(&HomeTexObj[TEXTURE_FontMan]);

	PosX = tw + 200;
	PosY = 250;
	tw = GXGetTexObjWidth(&HomeTexObj[TEXTURE_FontGale]);	//横サイズ
	th = GXGetTexObjHeight(&HomeTexObj[TEXTURE_FontGale]);	//縦サイズ
	SpriteSetup();
	SpritrSetScale(1.0f, 1.0f);
	TexSetSize(0, 0, tw, th);
	th = th / 2;
	tw = tw / 2;
	SpritrSetSize(PosX, PosY, tw, th);
	if (PosX <= GetPX() && GetPX() <= (PosX + tw))
	{
		if (PosY <= GetPY() && GetPY() <= (PosY + th))
		{
			SpriteSetColor(242, 255, 0, 255);
			g_nID = 2; // 女
			if (FadeFlag() == true)
			{
				g_SeneID = g_nID;
			}
		}
	}
	else
	{
		SpriteSetColor(255, 255, 255, 255);
		g_nID = 0;
	}
	SpriteDraw(&HomeTexObj[TEXTURE_FontGale]);
}

void HomeMan(void)
{
	SpriteSetup();
	SpriteSetColor(255, 255, 255, 255);
	SpritrSetScale(1.0f, 1.0f);
	TexSetSize(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	SpritrSetSize(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	SpriteDraw(&HomeTexObj[TEXTURE_HOMEMan]);
}

void HomeGale(void)
{
	SpriteSetup();
	SpriteSetColor(255, 255, 255, 255);
	SpritrSetScale(1.0f, 1.0f);
	TexSetSize(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	SpritrSetSize(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	SpriteDraw(&HomeTexObj[TEXTURE_HOMEGale]);
}

int GetPlayerID(void)
{
	return g_SeneID;
}