#ifndef _GAME_H_
#define _GAME_H_

#define NUMLOVE (30)

typedef enum _TEXTURE_GAME
{
	TEXTURE_NUM_GAME = 0,
	TEXTURE_NUMBER,
	TEXTURE_mens1_5,
	TEXTURE_girl1_4,
	TEXTURE_Love,
	TEXTURE_GAME_MAX
}TEXTURE_GAME;

void GameCase(void);
void GameInit(void);
void GameUninit(void);
void GameUpdate(void);
void GameDraw(void);
int GameGetFade(void);
int GameInit2(void);

void LoveSet(float PosX,float PosY);
void LoveUpdate(void);
void LoveDraw(void);

#endif // !_GAME_H_
