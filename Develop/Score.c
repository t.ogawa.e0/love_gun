#include <revolution.h>
#include <revolution/kpad.h>
#define DEMO_USE_MEMLIB
#include <demo.h>			//
#include <revolution/gx.h> 	// OSFatalを使用する場合includeが必要

#include "mem2allocator.h"
#include <math.h>
#include <stdio.h>			// sprintfなども使用できます
#include "sprite.h"
#include "Controller.h"

#include "main.h"
#include "Game.h"
#include "Score.h"
#include "Number.h"

static int g_nMax = 0;
static int count = 90;
static int nCount = 60;
static bool nEnd;
static int ScorCunt = 0;
static GXTexObj* g_pTex;

void ScoreInit(void)
{
	g_nMax = NumberInit(MAX_SCORE);
}

void ScoreInit2(void)
{
	ScorCunt = 0;
}

void ScoreUninit(void)
{

}

void ScoreUpdate(void)
{

}

void ScoreDraw(void)
{
	NumberSpriteSetColor(255, 0, 0, 255);
	NumberSpritrSetScale(0.5f, 0.5f);
	NumberTexSetSize(NUMBER_W, NUMBER_H, NUMBER_W, NUMBER_H);
	NumberSpritrSetSize(280, 200, NUMBER_W, NUMBER_H);
	NumberSetAnimasion(ScorCunt, 1, NUM_NUMBER, NUM_NUMBER_W);
	NumberSetDraw(g_pTex);
	NumberDraw(MAX_SCORE, g_nMax, true, true, ScorCunt);
}

int ScoreGetFade(void)
{
	return nEnd;
}

void ScorPlus(void)
{
	ScorCunt++;
}

void ScorMinus(void)
{
	if (ScorCunt != 0)
	{
		ScorCunt--;
	}
}

void ScorTexSet(GXTexObj* pTex)
{
	g_pTex = pTex;
}