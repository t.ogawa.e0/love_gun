//=======================================================================================
//
//    [ sprite.c ] スプライト描画モジュール
//
//		このファイルへ関数を作成する
//=======================================================================================
#include "main.h"
#include "sprite.h"

static float g_PolyX = 0.0f;
static float g_PolyY = 0.0f;
static float g_PolyW = 0.0f;
static float g_PolyH = 0.0f;
static int g_TexCutX = 0;
static int g_TexCutY = 0;
static int g_TexCutW = 0;
static int g_TexCutH = 0;
static float g_ScaleW = 1.0f;
static float g_ScaleH = 1.0f;

//=======================================================================================
// スプライト描画準備関数
//
// SpriteのDraw関数を呼ぶ前にコールする
//
// ※他のSetup関数が呼ばれた後、SpriteのDraw関数を呼ぶ場合には再度コールすること
//=======================================================================================
void SpriteSetup(void)
{
	//ポリゴン頂点の定義
	GXClearVtxDesc();
	GXSetVtxDesc(GX_VA_POS, GX_DIRECT);
	GXSetVtxDesc(GX_VA_CLR0, GX_DIRECT);
	GXSetVtxDesc(GX_VA_TEX0, GX_DIRECT);

	//頂点の属性の設定部分
	GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_POS, GX_POS_XYZ, GX_F32, 0);
	GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_CLR0, GX_CLR_RGB, GX_RGBA8, 0);
	GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX0, GX_TEX_ST, GX_F32, 0);	// F32


	// 使用するTEVステージ数の設定
	GXSetNumTevStages(1); // 1つ

	// カラーチャンネル数の設定
	GXSetNumChans(1); // 1つ

	// 利用可能にするテクスチャ座標数の設定
	GXSetNumTexGens(1); // つかう

	// TEVで使用するテクスチャーカラーとラスタライズカラーの設定
	GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR0A0);

	//TEVが上の要素のどのように合成するか？を設定する
	GXSetTevOp(GX_TEVSTAGE0, GX_MODULATE);
}


//=======================================================================================
// スプライト色設定関数
//
// スプライト色を指定する
// この関数が呼ばれた後のスプライト画像はすべてここで指定した色との乗算合成になる
//
// r ... 赤要素(0-255)
// g ... 緑要素(0-255)
// b ... 青要素(0-255)
// a ... α値   (0-255)
//=======================================================================================
void SpriteSetColor(u8 r, u8 g, u8 b, u8 a)
{
	spriteR = r;
	spriteG = g;
	spriteB = b;
	spriteA = a;
}

void SpritrSetSize( float dx, float dy,float dw, float dh)
{
	g_PolyX = dx;
	g_PolyY = dy;
	g_PolyW = dw;
	g_PolyH = dh;
}

// tx, ty ... テクスチャの切り取り左上座標
void TexSetSize(int tx, int ty,int tw, int th)
{
	g_TexCutX = tx;
	g_TexCutY = ty;
	g_TexCutW = tw;
	g_TexCutH = th;
}

void SpritrSetScale(float ScaleW,float ScaleH)
{
	g_ScaleW = ScaleW;
	g_ScaleH = ScaleH;
}

void SetAnimasion(int Count, int Flame, int Pattan, int Direction)
{
	int pattern = Count / Flame%Pattan;
	int patternV = pattern%Direction;
	int patternH = pattern / Direction;

	g_TexCutX = patternV*g_TexCutX;
	g_TexCutY = patternH*g_TexCutY;
}

//=======================================================================================
// スプライト描画関数
//
// pTex   ... テクスチャオブジェクトの先頭アドレス
// dx, dy ... スプライトの左上座標
// dw, dh ... スプライトの幅と高さ(テクスチャの切り取り幅と高さ)
// tx, ty ... テクスチャの切り取り左上座標
//=======================================================================================
void SpriteDraw(GXTexObj* pTex)
{
	u16 tw = GXGetTexObjWidth(pTex);	//横サイズ
	u16 th = GXGetTexObjHeight(pTex);	//縦サイズ

										//左上
	float u0 = (float)g_TexCutX / tw;
	float v0 = (float)g_TexCutY / th;

	//左下
	float u1 = (float)(g_TexCutX + g_TexCutW) / tw;
	float v1 = (float)(g_TexCutY + g_TexCutH) / th;

	//αブレンド設定
	GXSetBlendMode(GX_BM_BLEND, GX_BL_SRCALPHA, GX_BL_INVSRCALPHA,/*GX_BL_ONE*/GX_LO_CLEAR);

	//テクスチャマップ0番にテクスチャを設定
	GXLoadTexObj(pTex, GX_TEXMAP0);

	GXBegin(GX_QUADS, GX_VTXFMT0, 4);//セット開始

	GXPosition3f32(g_PolyX*g_ScaleW, g_PolyY*g_ScaleH, 0.0f);//頂点座標	頂点が先	
	GXColor4u8(spriteR, spriteG, spriteB, spriteA);//頂点カラーセット
	GXTexCoord2f32(u0, v0);//テクスチャ座標

	GXPosition3f32((g_PolyX + g_PolyW)*g_ScaleW, g_PolyY*g_ScaleH, 0.0f);//頂点座標
	GXColor4u8(spriteR, spriteG, spriteB, spriteA);//頂点カラーセット
	GXTexCoord2f32(u1, v0);//テクスチャ座標

	GXPosition3f32((g_PolyX + g_PolyW)*g_ScaleW, (g_PolyY + g_PolyH)*g_ScaleH, 0.0f);//頂点座標
	GXColor4u8(spriteR, spriteG, spriteB, spriteA);//頂点カラーセット
	GXTexCoord2f32(u1, v1);//テクスチャ座標

	GXPosition3f32(g_PolyX*g_ScaleW, (g_PolyY + g_PolyH)*g_ScaleH, 0.0f);//頂点座標
	GXColor4u8(spriteR, spriteG, spriteB, spriteA);//頂点カラーセット
	GXTexCoord2f32(u0, v1);//テクスチャ座標

	GXEnd();//セット終了
}
