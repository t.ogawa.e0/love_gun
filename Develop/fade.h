#ifndef _FADE_H_
#define _FADE_H_

#define FADE_SPEED (5)

typedef enum
{
	TEXTURE_NUM_FADE = 0,
	TEXTURE_FADE_MAX
}TEXTURE_FADE_DATA;


void FadeCase(int bFade);
void FadeInit(void);
void FadeUninit(void);
void FadeUpdate(int bFade);
void FadeDraw(void);

int GetCene(void);
void FadeON(int Fade);

#endif // !_FADE_H_
