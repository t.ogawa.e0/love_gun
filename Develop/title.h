#ifndef _TITLE_H_
#define _TITLE_H_

typedef enum
{
	TEXTURE_NUM_TITLE = 0,
	TEXTURE_TITLE_MAX
}TEXTURE_TITLE_DATA;

void TitleCase(void);
void TitleInit(void);
void TitleUninit(void);
void TitleUpdate(void);
void TitleDraw(void);
void TitleInit2(void);

#endif