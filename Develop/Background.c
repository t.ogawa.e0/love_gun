#include <revolution.h>
#include <revolution/kpad.h>
#define DEMO_USE_MEMLIB
#include <demo.h>			//
#include <revolution/gx.h> 	// OSFatalを使用する場合includeが必要

#include "mem2allocator.h"
#include <math.h>
#include <stdio.h>			// sprintfなども使用できます
#include "sprite.h"
#include "Controller.h"
#include "main.h"

#include "Background.h"


void BackgroundInit(void)
{

}

void BackgroundUninit(void)
{

}

void BackgroundUpdate(void)
{

}

void BackgroundDraw(GXTexObj* pTex)
{
	// 背景
	SpriteSetup();
	SpriteSetColor(255, 255, 255, 255);
	SpritrSetScale(1.0f, 1.0f);
	TexSetSize(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	SpritrSetSize(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	SpriteDraw(pTex);
}