//----------------------------------------------
//リモコンスピーカ再生　V0.99
//----------------------------------------------

#ifndef __SPDDEMO_H__
#define __SPDDEMO_H__


void  SpeakerOnCallback ( s32 chan, s32 result );//	スピーカーON確認　コールバック関数

void	InitRemocon();							//リモコン再生初期化
u8		GetSpeakerActive(int index);			//リモコン状態取得







//-------------------------------------
static void  UpdateSpeaker     ( OSAlarm *alarm, OSContext *context ); //再生処理本体
static void  SpeakerCallback   ( s32 chan, s32 result );//
static void  SpeakerOffCallback( s32 chan, s32 result );//　//
static void  ConnectCallback   ( s32 chan, s32 reason );//　//
static void  ExtensionCallback ( s32 chan, s32 result );//　本ソースでは未使用
//-------------------------------------


#endif

//EOF//////////