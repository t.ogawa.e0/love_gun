#include <revolution.h>
#include <revolution/kpad.h>
#define DEMO_USE_MEMLIB
#include <demo.h>			//
#include <revolution/gx.h> 	// OSFatalを使用する場合includeが必要

#include "mem2allocator.h"
#include <math.h>
#include <stdio.h>			// sprintfなども使用できます
#include <stdlib.h>
#include "main.h"
#include "sprite.h"
#include "Controller.h"
#include "Enemy.h"
#include "Player.h"
#include "home.h"
#include "Score.h"
#include "Game.h"

typedef struct _ENEMY
{
	float PosX, PosY, PosW, PosH; // エネミーポリゴン情報
	int Life, MaxLife; // HP
	int Attac; // 攻撃力
	bool bLipop; // リポップフラグ
	bool bHit;
	u8 r, g, b, a;
	float ScaleX, ScaleY;
	int TexX, TexY, TexW, TexH;
	int ID;
	int TexNunBer;
	int Spped;
}ENEMY;

static ENEMY g_aEnemy[MAX_ENEMY];
static int EnemyRelese = 0;

void EnemyInit(GXTexObj* pTex, int nCount, int ID, int TexNunBer)
{
	u16 TexWiz;	//横サイズ
	u16 TexHei;	//縦サイズ
	int nCath = 0;
	static int nNotSPeedNum[5];
	int SppedCase[10] =
	{
		1,2,4,6,8,10,12,13,14,15
	};
	int PosCase[10] =
	{
		-50,-80,-100,-200,-150,-135,-154,-342,-420,-80
	};

	g_aEnemy[nCount].bLipop = true;
	g_aEnemy[nCount].bHit = false;
	g_aEnemy[nCount].PosH = g_aEnemy[nCount].PosW = g_aEnemy[nCount].PosX = g_aEnemy[nCount].PosY = 0.0f;
	g_aEnemy[nCount].Attac = 1;
	g_aEnemy[nCount].Life = 1;
	for (;;)
	{
		nCath = rand() % (int)(sizeof(SppedCase) / 4);
		if (nNotSPeedNum[0] != nCath&&nNotSPeedNum[1] != nCath&&nNotSPeedNum[2] != nCath&&nNotSPeedNum[3] != nCath&&nNotSPeedNum[4] != nCath)
		{
			nNotSPeedNum[4] = nNotSPeedNum[3];
			nNotSPeedNum[3] = nNotSPeedNum[2];
			nNotSPeedNum[2] = nNotSPeedNum[1];
			nNotSPeedNum[1] = nNotSPeedNum[0];
			nNotSPeedNum[0] = nCath;
			break;
		}
	}
	g_aEnemy[nCount].PosX = PosCase[nCath];
	TexWiz = GXGetTexObjWidth(pTex);	//横サイズ
	TexHei = GXGetTexObjHeight(pTex);	//縦サイズ
	g_aEnemy[nCount].Spped = SppedCase[nCath];
	g_aEnemy[nCount].MaxLife = 1;

	g_aEnemy[nCount].ScaleX = g_aEnemy[nCount].ScaleY = 1.0f;
	g_aEnemy[nCount].TexX = g_aEnemy[nCount].TexY = 0;
	g_aEnemy[nCount].TexW = TexWiz;
	g_aEnemy[nCount].TexH = TexHei;
	g_aEnemy[nCount].PosW = TexWiz / 1.8;
	g_aEnemy[nCount].PosH = TexHei / 1.8;
	if (ID == 2)
	{
		g_aEnemy[nCount].PosW = TexWiz / 2.3f;
		g_aEnemy[nCount].PosH = TexHei / 2.3f;
	}
	if (ID == 1)
	{
		g_aEnemy[nCount].r = 90;
		g_aEnemy[nCount].g = 172;
		g_aEnemy[nCount].b = 255;
		g_aEnemy[nCount].a = 255;
	}
	if (ID == 2)
	{
		g_aEnemy[nCount].r = 255;
		g_aEnemy[nCount].g = 90;
		g_aEnemy[nCount].b = 148;
		g_aEnemy[nCount].a = 255;
	}
	g_aEnemy[nCount].ID = ID;
	g_aEnemy[nCount].TexNunBer = TexNunBer;
	g_aEnemy[nCount].PosY = SCREEN_HEIGHT / 2.5;
}

void EnemyUninit(void)
{

}

void EnemyUpdate(void)
{	
	int nCount = 0;
	int nCath = 0;
	int PosCase[10] =
	{
		-50,-80,-100,-200,-150,-135,-154,-342,-420,-80
	};
	int SppedCase[10] =
	{
		1,2,4,6,8,10,12,13,14,15
	};
	static int nNotSPeedNum[5];

	for (nCount = 0; nCount < MAX_ENEMY; nCount++)
	{
		if (g_aEnemy[nCount].bHit == false)
		{
			g_aEnemy[nCount].PosX += g_aEnemy[nCount].Spped;
		}
		else
		{
			g_aEnemy[nCount].PosX += 50;
		}

	}

	if (PlayerUpdate() == true)
	{
		for (nCount = 0; nCount < MAX_ENEMY; nCount++)
		{
			if (g_aEnemy[nCount].PosY <= GetPY() && GetPY() <= (g_aEnemy[nCount].PosY + g_aEnemy[nCount].PosH))
			{
				if (g_aEnemy[nCount].PosX <= GetPX() && GetPX() <= (g_aEnemy[nCount].PosX + g_aEnemy[nCount].PosW))
				{
					if (GetPlayerID() == g_aEnemy[nCount].ID)
					{
						if (g_aEnemy[nCount].bHit == false)
						{
							g_aEnemy[nCount].r = 250;
							g_aEnemy[nCount].g = 152;
							g_aEnemy[nCount].b = 255;
							ScorPlus();
							LoveSet(g_aEnemy[nCount].PosX, g_aEnemy[nCount].PosY);
							g_aEnemy[nCount].bHit = true;
						}
					}
					else
					{
						if (g_aEnemy[nCount].bHit == false)
						{
							g_aEnemy[nCount].r = 130;
							g_aEnemy[nCount].g = 17;
							g_aEnemy[nCount].b = 183;
							ScorMinus();
							g_aEnemy[nCount].bHit = true;
						}
					}
				}
			}
		}
	}

	for (nCount = 0; nCount < MAX_ENEMY; nCount++)
	{
		if (SCREEN_WIDTH <= g_aEnemy[nCount].PosX)
		{
			g_aEnemy[nCount].bHit = false;
			if (g_aEnemy[nCount].ID == 1)
			{
				g_aEnemy[nCount].r = 90;
				g_aEnemy[nCount].g = 172;
				g_aEnemy[nCount].b = 255;
				g_aEnemy[nCount].a = 255;
			}
			if (g_aEnemy[nCount].ID == 2)
			{
				g_aEnemy[nCount].r = 255;
				g_aEnemy[nCount].g = 90;
				g_aEnemy[nCount].b = 148;
				g_aEnemy[nCount].a = 255;
			}
			for (;;)
			{
				nCath = rand() % (int)(sizeof(SppedCase) / 4);
				if (nNotSPeedNum[0] != nCath&&nNotSPeedNum[1] != nCath&&nNotSPeedNum[2] != nCath&&nNotSPeedNum[3] != nCath&&nNotSPeedNum[4] != nCath)
				{
					nNotSPeedNum[4] = nNotSPeedNum[3];
					nNotSPeedNum[3] = nNotSPeedNum[2];
					nNotSPeedNum[2] = nNotSPeedNum[1];
					nNotSPeedNum[1] = nNotSPeedNum[0];
					nNotSPeedNum[0] = nCath;
					break;
				}
			}
			g_aEnemy[nCount].PosX = PosCase[nCath];
			g_aEnemy[nCount].Spped = SppedCase[nCath];
		}
	}
}

void EnemyDraw(GXTexObj* pTex, int nCount)
{
	SpriteSetup();
	SpriteSetColor(g_aEnemy[nCount].r, g_aEnemy[nCount].g, g_aEnemy[nCount].b, g_aEnemy[nCount].a);
	SpritrSetScale(g_aEnemy[nCount].ScaleX, g_aEnemy[nCount].ScaleY);
	TexSetSize(g_aEnemy[nCount].TexX, g_aEnemy[nCount].TexY, g_aEnemy[nCount].TexW, g_aEnemy[nCount].TexH);
	SpritrSetSize(g_aEnemy[nCount].PosX, g_aEnemy[nCount].PosY, g_aEnemy[nCount].PosW, g_aEnemy[nCount].PosH);
	SpriteDraw(pTex);
}

void EnemyRipoop(void)
{
	int nCount = 0;
/*
	if (EnemyRelese <= 1)
	{
		nCount = rand() % MAX_ENEMY;
		if (MAX_ENEMY <= nCount) { nCount = 0; }
		if (g_aEnemy[nCount].bLipop == false)
		{
			g_aEnemy[nCount].bLipop = true;
		}
		EnemyRelese++;
	}*/
}

int EnemyGetTexNunBer(int NunBer)
{
	return g_aEnemy[NunBer].TexNunBer;
}