#ifndef _MAN_H_
#define _MAN_H_

void ManInit(void);
void ManUninit(void);
void ManUpdate(void);
void ManDraw(void);

#endif // !_MAN_H_
