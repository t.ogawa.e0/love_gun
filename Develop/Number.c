#include "main.h"

#include <revolution.h>
#include <revolution/kpad.h>
#define DEMO_USE_MEMLIB
#include <demo.h>			//
#include <revolution/gx.h> 	// OSFatalを使用する場合includeが必要

#include "mem2allocator.h"
#include <math.h>
#include <stdio.h>			// sprintfなども使用できます
#include "sprite.h"
#include "Controller.h"
#include "Number.h"

static GXTexObj* g_pTex;
static int g_Count, g_Direction, g_Flame, g_Pattan;
static float g_dx, g_dy, g_dw, g_dh;
static int g_tx, g_ty, g_tw, g_th;
static float g_ScaleW, g_ScaleH;
static u8 g_r, g_g, g_b, g_a;

int NumberInit(int Digit)
{
	int i = 0;
	int MaxScore = PUSYU_SCORE;

	for (i = 1; i < Digit; i++)
	{
		MaxScore *= PUSYU_SCORE;
	}
	MaxScore--;

	return MaxScore;
}

void NumberUninit(void)
{

}

void NumberUpdate(void)
{

}

void NumberDraw(int Digit, int Max, bool Zero, bool LeftAlignment, int Score)
{
	float PosX = g_dx;
	int i = 0;

	//左詰め対応
	if (LeftAlignment == true)
	{
		int Set = Score;

		Digit = 1;
		for (;;)
		{
			Set /= PUSYU_SCORE;
			if (Set == 0)
			{
				break;
			}
			Digit++;
		}
	}

	Score = min(Max, Score);

	for (i = Digit - 1; i >= 0; i--)
	{
		// 1234567
		int n = Score % PUSYU_SCORE;
		// 123456

		g_dx = PosX + i * g_dw;

		SpriteSetup();
		SpriteSetColor(g_r, g_g, g_b, g_a);
		SpritrSetScale(g_ScaleW, g_ScaleH);
		TexSetSize(g_tx, g_ty, g_tw, g_th);
		SpritrSetSize(g_dx, g_dy, g_dw, g_dh);
		SetAnimasion(n, g_Flame, g_Pattan, g_Direction);
		SpriteDraw(g_pTex);

		Score /= PUSYU_SCORE;
		// 12345.6
		if (!Zero&&Score == 0)
		{
			{
				break;
			}
		}
	}
}

void NumberSpriteSetColor(u8 r, u8 g, u8 b, u8 a)
{
	g_r = r;
	g_g = g;
	g_b = b;
	g_a = a;
}
void NumberSpritrSetScale(float ScaleW, float ScaleH)
{
	g_ScaleW = ScaleW;
	g_ScaleH = ScaleH;
}
void NumberTexSetSize(int tx, int ty, int tw, int th)
{
	g_tx = tx;
	g_ty = ty;
	g_tw = tw;
	g_th = th;
}
void NumberSpritrSetSize(float dx, float dy, float dw, float dh)
{
	g_dx = dx;
	g_dy = dy;
	g_dw = dw;
	g_dh = dh;
}
void NumberSetAnimasion(int Count, int Flame, int Pattan, int Direction)
{
	g_Count = Count;
	g_Flame = Flame;
	g_Pattan = Pattan;
	g_Direction = Direction;
}
void NumberSetDraw(GXTexObj* pTex)
{
	g_pTex = pTex;
}