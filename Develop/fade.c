#include <revolution.h>
#include <revolution/kpad.h>
#define DEMO_USE_MEMLIB
#include <demo.h>			//
#include <revolution/gx.h> 	// OSFatalを使用する場合includeが必要

#include "mem2allocator.h"
#include <math.h>
#include <stdio.h>			// sprintfなども使用できます
#include "main.h"
#include "sprite.h"
#include "Controller.h"
#include "fade.h"
#include "Game.h"

static TPLPalettePtr atexPal;				//テクスチャパレットを表示する型の変数
static GXTexObj FadeTexObj[TEXTURE_FADE_MAX];	//テクスチャオブジェクトを保存する配列

static int g_nScene = SCREENROOC;
static bool g_Fard = false;
static int g_arufa = 0;
static bool g_FadeLok = false;

void FadeCase(int bFade)
{
	FadeUpdate(bFade);
	FadeDraw();
}
void FadeInit(void)
{
	int nCount;
	// TPLファイルノロード
	TPLGetPalette(&atexPal, "fade.tpl");

	//ロードエラーチェック
	ASSERTMSG(TEXTURE_FADE_MAX == atexPal->numDescriptors, "\nTPL内画像総数とNUM_MAXの値が一致していない\n(numDescriptors==%d)!=(NUM_MAX=%d)\n", atexPal->numDescriptors, TEXTURE_FADE_MAX);

	//TPLからテクスチャオブジェクトを取り出す
	for (nCount = 0; nCount < atexPal->numDescriptors; nCount++)
	{
		TPLGetGXTexObjFromPalette(atexPal, &FadeTexObj[nCount], nCount);
	}
}
void FadeUninit(void)
{

}
void FadeUpdate(int bFade)
{
	// フェード判定
	if (g_Fard == false)
	{
		if (bFade == true)
		{
			g_Fard = FadeFlag();
		}
	}

	if (g_Fard == true)
	{
		if (g_FadeLok == false)
		{
			if (g_arufa <= 255)
			{
				g_arufa += FADE_SPEED;
			}

			if (g_arufa >= 255)
			{
				g_nScene++;
				if (g_nScene>3)
				{
					g_nScene = 0;
				}
				g_FadeLok = true;
			}
		}
		if (g_FadeLok == true)
		{
			if (0 <= g_arufa)
			{
				g_arufa -= FADE_SPEED;
			}
			if (0 >= g_arufa)
			{
				g_Fard = false;
				g_arufa = 0;
				g_FadeLok = false;
			}
		}
	}
}

void FadeON(int Fade)
{
	g_Fard=Fade;
	
}
void FadeDraw(void)
{
	if (g_Fard == true)
	{
		SpriteSetup();
		SpriteSetColor(255, 255, 255, g_arufa);
		SpritrSetScale(1.0f, 1.0f);
		TexSetSize(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		SpritrSetSize(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		SpriteDraw(&FadeTexObj[TEXTURE_NUM_FADE]);
	}
}

int GetCene(void)
{
	return g_nScene;
}