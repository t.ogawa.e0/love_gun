//=======================================================================================
//
//		main.c
//    	AT12A242　(名前)	2017/05/19
//
//=======================================================================================
#include <revolution.h>
#include <revolution/kpad.h>
#define DEMO_USE_MEMLIB
#include <demo.h>			//
#include "mem2allocator.h"

/*	サウンドサンプル */
#include "YSound.h"			
#include "wpad_spdemo.h"
#include "sample_se.h"	//コンバートすると作成されるヘッダファイル


#include <math.h>
#include <revolution/gx.h> 	// OSFatalを使用する場合includeが必要
#include <stdio.h>			// sprintfなども使用できます
#include "primitive.h"
#include "sprite.h"
#include "title.h"
#include "main.h"
#include "Controller.h"
#include "home.h"
#include "Game.h"
#include "result.h"
#include "rectele.h"
#include "fade.h"
#include "Score.h"


//------------------------------------------------------------------------
//	グローバル変数

static GXColor BG_COLOR = { 80,80,80 };
static TPLPalettePtr texPal;				//テクスチャパレットを表示する型の変数
static	SoundCounter = 0;//for Sound Sample Test

//=======================================================================================
//    main関数
//=======================================================================================
void main()
{
	int nCene = SCREENROOC;
	bool bFade=false;

	Init();

	//-------------------------------------
	//	ゲームループ
	while (1)
	{
		Update();

		DEMOBeforeRender();

		switch (nCene)
		{
		case 0:
			TitleCase();
			HomeInit2();
			ScoreInit2();
			bFade=true;
			break;
		case 1:
			HomeCase();
			GameInit2();
			ScoreInit2();
			bFade=HomeGetFade();
			break;
		case 2:
			GameCase();
			FadeON(bFade);
			bFade=GameGetFade();
			break;
		case 3:
			ResultCase();
			bFade=true;
			break;
		default:
			break;
		}
		RecteleCase();
		nCene = GetCene();
		FadeCase(bFade);

		DEMODoneRender();
	}
	//end
	Uninit();
	OSHalt("End of Sample");
}

void Init(void)
{
	//Wiiの初期化
	DEMOInit(NULL);

	//KPADRライブラリ初期化
	WPADRegisterAllocator(Mem2Alloc32, Mem2Free32);
	
	/*	サウンドサンプル */
   	// YSound初期化
    InitRemocon();
	YSound_Init(Mem2Alloc32, Mem2Free32);
	
	// SEの読み込み(オンメモリ)
	YSound_SetOnMemoryVolume(100);
	YSound_LoadOnMemoryFile("sample_se.spt", "sample_se.spd");
	
	//ストリーミング再生開始
	while( YSound_GetStreamState() != YSOUND_STREAM_STATE_NONE){}
	YSound_SetStreamVolume(50);
	YSound_StartStream("morning-cafe_l.dsp", "morning-cafe_r.dsp");

	ControllerInit();
	TitleInit();
	ScoreInit();
	HomeInit();
	GameInit();
	ResultInit();
	RecteleInit();
	FadeInit();
}
void Uninit(void)
{
	/*********************/
	/*	サウンドサンプル */
	/*********************/
	// YSoundの終了処理
	YSound_Uninit();
	/*********************/
	
	TitleUninit();
	ControllerUninit();
	HomeInit();
	GameUninit();
	ResultUninit();
	RecteleUninit();
	FadeUninit();
}
void Update(void)
{
	// プロジェクション行列変数
	Mtx44 projMatrix;

	//2D用プロジェクション行列=正射影行列作成
	MTXOrtho(projMatrix, 0, SCREEN_HEIGHT, 0, SCREEN_WIDTH, 0.0f, 1.0f);

	//作成した行列をWiiへセット
	GXSetProjection(projMatrix, GX_ORTHOGRAPHIC);

	//画面を背景色でクリア
	GXSetCopyClear(BG_COLOR, GX_MAX_Z24);

	ControllerUpdate();
}
void Draw(void)
{

}