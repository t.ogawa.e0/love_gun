#ifndef _SCORE_H_
#define _SCORE_H_

#define MAX_SCORE (3)

void ScoreInit(void);
void ScoreInit2(void);
void ScoreUninit(void);
void ScoreUpdate(void);
void ScoreDraw(void);
int ScoreGetFade(void);

void ScorPlus(void);
void ScorMinus(void);
void ScorTexSet(GXTexObj* pTex);


#endif // !_SCORE_H_
