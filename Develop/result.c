#include <revolution.h>
#include <revolution/kpad.h>
#define DEMO_USE_MEMLIB
#include <demo.h>			//
#include <revolution/gx.h> 	// OSFatalを使用する場合includeが必要

#include "mem2allocator.h"
#include <math.h>

#include <stdio.h>			// sprintfなども使用できます

#include "main.h"
#include "sprite.h"
#include "Controller.h"
#include "result.h"
#include "home.h"
#include "Score.h"

static TPLPalettePtr texPal;				//テクスチャパレットを表示する型の変数
static GXTexObj ResultTexObj[TEXTURE_RESULT_MAX];	//テクスチャオブジェクトを保存する配列

void ResultCase(void)
{
	ResultUpdate();
	ResultDraw();
}
void ResultInit(void)
{
	int nCount;

	// TPLファイルノロード
	TPLGetPalette(&texPal, "result.tpl");

	//ロードエラーチェック
	ASSERTMSG(TEXTURE_RESULT_MAX == texPal->numDescriptors, "\nTPL内画像総数とNUM_MAXの値が一致していない\n(numDescriptors==%d)!=(NUM_MAX=%d)\n", texPal->numDescriptors, TEXTURE_RESULT_MAX);

	//TPLからテクスチャオブジェクトを取り出す
	for (nCount = 0; nCount < texPal->numDescriptors; nCount++)
	{
		TPLGetGXTexObjFromPalette(texPal, &ResultTexObj[nCount], nCount);
	}
}
void ResultUninit(void)
{
	// テクスチャパレットを解放
	TPLReleasePalette(&texPal);
}
void ResultUpdate(void)
{

}
void ResultDraw(void)
{
	switch (GetPlayerID())
	{
	case 1:
		SpriteSetup();
		SpriteSetColor(255, 255, 255, 255);
		SpritrSetScale(1.0f, 1.0f);
		TexSetSize(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		SpritrSetSize(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		SpriteDraw(&ResultTexObj[TEXTURE_RESULT_MAN]);
		break;
	case 2:
		SpriteSetup();
		SpriteSetColor(255, 255, 255, 255);
		SpritrSetScale(1.0f, 1.0f);
		TexSetSize(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		SpritrSetSize(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		SpriteDraw(&ResultTexObj[TEXTURE_RESULT_GAL]);
		break;
	default:
		break;
	}
	ScoreDraw();
}