#ifndef _BACKGROUND_H_
#define _BACKGROUND_H_

void BackgroundInit(void);
void BackgroundUninit(void);
void BackgroundUpdate(void);
void BackgroundDraw(GXTexObj* pTex);

#endif // !_BACKGROUND_H_
