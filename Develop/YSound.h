#ifndef _STREAM_SOUND_H_
#define _STREAM_SOUND_H_

//
// サウンドシステム共通
//
///////////////////////////////////////////////////////////

// メモリアロケート用関数ポインタ定義
typedef void * ( *YSoundAlloc )( u32  size );
typedef u8     ( *YSoundFree  )( void *ptr );

// サウンドシステムの初期化
// 引数にはMEM2メモリアロケート・フリー関数のアドレス
void YSound_Init(YSoundAlloc alloc, YSoundFree free);

// サウンドシステムの終了処理
void YSound_Uninit(void);


//
// ストリーミングサウンド（BGM）
//
///////////////////////////////////////////////////////////

// ストリーム制御状態列挙型
typedef enum YSoundStreamState_tag
{
    YSOUND_STREAM_STATE_NONE,
    YSOUND_STREAM_STATE_INITIALIZING,
    YSOUND_STREAM_STATE_STARTED,
    YSOUND_STREAM_STATE_STOPPING,
    YSOUND_STREAM_STATE_STOPPED,
    YSOUND_STREAM_STATE_PAUSE,
    YSOUND_STREAM_STATE_MAX
}YSoundStreamState;

// サウンドのストリーム再生の状態を取得する
YSoundStreamState YSound_GetStreamState(void);

// サウンドのストリーム再生
// 引数には左チャンネル・右チャンネルのADPCMファイル名
void YSound_StartStream(const char* pFileNameL, const char* pFileNameR);

// サウンドのストリーム再生停止
void YSound_StopStream(void);

// サウンドのストリーム再生一時停止
void YSound_PauseStream(void);

// サウンドのストリーム再生再開
void YSound_ResumeStream(void);

// サウンドのストリーミング再生ボリュームの設定
// 引数には0(無音)〜100(生音量)
void YSound_SetStreamVolume(s32 volume);

// サウンドのストリーミング再生ボリュームの取得
s32 YSound_GetStreamVolume(void);

//
// オンメモリサウンド(SE)
//
///////////////////////////////////////////////////////////

// オンメモリ用サウンドデータの読み込み
// 引数にはサウンドテーブル・サウンドデータのファイル名
void YSound_LoadOnMemoryFile(const char* fileNameTable, const char* fileNameData);

// サウンドのオンメモリ再生
// 引数には再生したいサウンドのテーブルインデックス
// 戻り値はオンメモリサウンドの管理番号
s32 YSound_Play(u32 index);

s32 YSound_PlaySpeaker(u32 index);		//リモコン版

// 全オンメモリサウンドの停止
void YSound_StopAll(void);

// オンメモリサウンドの停止
// 引数はオンメモリサウンドの管理番号
void YSound_Stop(s32 onMemoryInfoIndex);

// オンメモリループサウンドの停止
// ※ループを停止してループエンドではなくサウンドの終りまで再生して停止する
// 引数はオンメモリサウンドの管理番号
void YSound_StopLooping(s32 onMemoryInfoIndex);

// オンメモリサウンドの再生ボリューム設定
void YSound_SetOnMemoryVolume(s32 volume);

// オンメモリサウンドの再生ボリューム取得
s32 YSound_GetOnMemoryVolume(void);

#endif // _STEREAM_SOUND_H_
