#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_

void ControllerInit(void);
void ControllerUninit(void);
void ControllerUpdate(void);
void ControllerDraw(void);

float GetPX(void);
float GetPY(void);
int FadeFlag(void);
int KeyB(void);

#endif // !_CONTROLLER_H_
