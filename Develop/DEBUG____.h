#ifndef _DEBUG___H_
#define _DEBUG___H_

typedef int TPLPalettePtr;
typedef int GXTexObj;
typedef int Vec2;
typedef int f32;
typedef int KPADStatus;
typedef int GXColor;
typedef int GX_MAX_Z24;
typedef int Mtx44;
typedef int GX_ORTHOGRAPHIC;
typedef int WPAD_CHAN0;
typedef int KPAD_BUTTON_A;
typedef int BOOL;
typedef int u8;
typedef int u16;

typedef enum EDEBUG_
{
	GX_BM_BLEND = 0,
	GX_BL_SRCALPHA,
	GX_BL_INVSRCALPHA,
	GX_LO_CLEAR,
	GX_TEXMAP0,
	GX_QUADS,
	GX_VTXFMT0,
	GX_VA_POS,
	GX_POS_XYZ,
	GX_CLR_RGB,
	GX_F32,
	GX_RGBA8,
	GX_TEX_ST,
	GX_TEVSTAGE0,
	GX_TEXCOORD0,
	GX_COLOR0A0,
	GX_MODULATE,
	GX_VA_CLR0,
	GX_DIRECT,
	GX_VA_TEX0,
	GX_RGB8,
	GX_TEXCOORD_NULL,
	GX_TEX_DISABLE,
	GX_PASSCLR,
	GX_POINTS,
	GX_LINES,
	GX_LINESTRIP,
	GX_TRIANGLESTRIP,
	GX_TRIANGLEFAN,
	GX_TO_ZERO,

};

#define TRUE 1
#define FALSE 0

#endif // !_DEBUG___H_

