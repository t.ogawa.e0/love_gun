//----------------------------------------------
//リモコンスピーカ再生　V0.99
//----------------------------------------------
#include <string.h>

#include <demo.h>
#include <revolution/mem.h>
#include <revolution/mix.h>
#include <revolution/sp.h>
#include <revolution/wenc.h>
#include <revolution/wpad.h>
#include <revolution/kpad.h>

#include "wpad_spdemo.h"



//****************************************************************************
//変数まわりはSDKサンプルの物がそのまま残っているので未使用のものも多い。
//適当に整理して使うこと。
//****************************************************************************
/*---------------------------------------------------------------------------*
  SP
 *---------------------------------------------------------------------------*/

// data
#define SPT_FILE "/spdemo/spdemo.spt"
#define SPD_FILE "/spdemo/spdemo.spd"

#define SFX_MENU            0 // not used
#define SFX_DRUM            1 // chan 0
#define SFX_GUNSHOT         2 // chan 1
#define VOICE_NGC_MAN       3 // chan 2
#define VOICE_NGC_WOMAN     4 // chan 3
#define SFX_HONK_LOOPED     5 // not used

static SPSoundTable *SpTable;





// application-layer voice abstraction
typedef struct 
{
    AXVPB        *voice;
    SPSoundEntry *entry;
    s32           chan;

} VoiceInfo;

static VoiceInfo vInfo[AX_MAX_VOICES];

// function prototypes
static void  AudioFrameCallback  ( void );
static void *LoadFileIntoRam     ( char *path );

static void       InitVoice          ( void );
static void       PlaySfx            ( s32 chan );
static void       StopSfx            ( s32 chan );
static VoiceInfo *GetVoice           ( void );
static void       DropVoiceCallback  ( void *p );
    
/*---------------------------------------------------------------------------*
  WPAD
 *---------------------------------------------------------------------------*/

// allocator functions for WPAD
static void *myAlloc ( u32 size );
static u8    myFree  ( void *ptr );


// Speaker Status
typedef struct SpeakerInfo
{
    u8           active;
    WENCInfo     encInfo;
    BOOL         first;
    BOOL         last;
    
} SpeakerInfo;

typedef union Status
{
    WPADStatus      cr;
    WPADFSStatus    fs;
    WPADCLStatus    cl;
} Status;

// Controller Status
typedef struct ContInfo
{
    u32          type;
    s32          status;
    Status       currStat;
    Status       prevStat;
    SpeakerInfo  Speakers;
    
    u8           play;
    
} ContInfo;

static ContInfo  info[WPAD_MAX_CONTROLLERS];

// Periodic Alarms for audio streaming
static OSAlarm  SpeakerAlarm;

// Audio buffers
#define SAMPLES_PER_AUDIO_PACKET  40
#define AUDIO_PACKET_LEN          20 // SAMPLES_PER_AUDIO_PACKET / 2

/*---------------------------------------------------------------------------*
  etc.
 *---------------------------------------------------------------------------*/

// Exp Heap
static MEMHeapHandle  hExpHeap;

// function prototypes
static void  initialize            ( void );
static void  RenderOperation       ( void );
static void  RenderControllerStatus( void );




/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
#define	MAX_CONTROLER	(2)		//コントローラ数



//-------------------------------------------------------------//
//	リモコン初期化
//-------------------------------------------------------------//
void	InitRemocon()
{
	int i;

    for (i = 0; i < MAX_CONTROLER; i++)
    {	//リモコン接続時に呼び出されるコールバック登録
        KPADSetConnectCallback((s32)i, ConnectCallback);
        //リモコンのスピーカーをON
	//	WPADControlSpeaker(i, WPAD_SPEAKER_ON, SpeakerOnCallback);        
    }
 
	//アラームの設定＆スピーカ処理用コールバックの登録
	//これ以後WPAD_STRM_INTERVAL経過ごとにOSが勝手にUpdateSpeaker()を呼び出す。
    OSCreateAlarm(&SpeakerAlarm);
    OSSetPeriodicAlarm(&SpeakerAlarm, OSGetTime(), WPAD_STRM_INTERVAL, UpdateSpeaker);
}

//*********************************************************
//---------------------------------------------------------
//リモコンの状態を取得
//---------------------------------------------------------
//********************************************************
u8	GetSpeakerActive(int index)
{
	return info[index].Speakers.active;
}


//////////////////////////////////////////////////////////////////////////////////
//	これが一番重要な部分
/*---------------------------------------------------------------------------*
 * Name        : UpdateSpeaker
 * Description : 一定周期ごとに呼び出されるコールバック
 * Arguments   : 
 * Returns     : None.
 *---------------------------------------------------------------------------*/
//////////////////////////////////////////////////////////////////////////////////
static void UpdateSpeaker( OSAlarm *alarm, OSContext *context )
{
#pragma unused(alarm, context)

    s16   pcmData[SAMPLES_PER_AUDIO_PACKET];
    u8    encData[AUDIO_PACKET_LEN];
    u32   flag;
    s32   chan;
    BOOL  intr;

	//再生データがあるかチェック
    if (SAMPLES_PER_AUDIO_PACKET <= AXRmtGetSamplesLeft())
    {
       
        for (chan = 0; chan <  MAX_CONTROLER; chan++)
        {
            //音声データをAX内部バッファからpcmData内に取り出す
            AXRmtGetSamples(chan, pcmData, SAMPLES_PER_AUDIO_PACKET);

            if (info[chan].Speakers.active)
            {	//割り込みを禁止する
                intr = OSDisableInterrupts();
                //リモコンへデータを転送できるかチェックする
                if (WPADCanSendStreamData(chan))
                	{//WENCGetEncodeData()に初回のエンコードかどうかを教えるflagをセットするための処理
                    flag = (info[chan].Speakers.first) ? (u32)WENC_FLAG_FIRST : (u32)WENC_FLAG_CONT;
                    if (info[chan].Speakers.first)
                    {
                        info[chan].Speakers.first = FALSE;//初回じゃないよ
                    }
                    //pcmDataに取り出したデータをスピーカ用にエンコード
                    WENCGetEncodeData(&info[chan].Speakers.encInfo,
                                      flag, //ここで初回か否かのフラグが設定される
                                      pcmData,
                                      SAMPLES_PER_AUDIO_PACKET,
                                      encData);
                    //エンコードされたデータをスピーカへ転送
                    WPADSendStreamData(chan, encData, AUDIO_PACKET_LEN);

                //    OSReport("アラームです。\n");
                }
                //割り込みの許可
                OSRestoreInterrupts(intr);
            }
        }
        //AX内部バッファの転送元アドレスを勧める
        AXRmtAdvancePtr(SAMPLES_PER_AUDIO_PACKET);
    }
    else
    {
//        OSReport("アラーム回避です。\n");
    }
}

/////////////////////////////////////////////////////////////////////////
/*---------------------------------------------------------------------------*
 * Name        : PlaySfx
 * Description : 音声再生部分　YSound_Play(）の内容とほぼ同じ
 * Arguments   : chan
 * Returns:    : None.
 *---------------------------------------------------------------------------*/
/////////////////////////////////////////////////////////////////////////
/*static void PlaySfx( s32 chan )
{
    VoiceInfo *v;
    BOOL       old;

    old = OSDisableInterrupts();

    v = GetVoice();
    if (v)
    {
        v->voice = AXAcquireVoice(15, DropVoiceCallback, 0);
        if (v->voice)
        {
            v->entry = SPGetSoundEntry(SpTable, (u32)(chan + 1));
            v->chan  = chan;
            
            SPPrepareSound(v->entry, v->voice, (v->entry)->sampleRate);
            
            MIXInitChannel(v->voice, 0, 0, -960, -960, -960, 64, 127, -960);
        	
        //--------------------------------------------------------------
        	//リモコンのスピーカー設定
        	//サンプルなのでリモコンごとに設定を変えているのでたくさんある。
            switch(chan)
            {
                case 0:
                    MIXRmtSetVolumes(v->voice, 0, 0, -960, -960, -960, -960, -960, -960, -960);
                    break;
                case 1:
                    MIXRmtSetVolumes(v->voice, 0, -960, 0, -960, -960, -960, -960, -960, -960);
                    break;
                case 2:
                    MIXRmtSetVolumes(v->voice, 0, -960, -960, 0, -960, -960, -960, -960, -960);
                    break;
                default:
                    MIXRmtSetVolumes(v->voice, 0, -960, -960, -960, 0, -960, -960, -960, -960);
                    break;
            }
        	
        	//スピーカで再生を行うよう設定
            AXSetVoiceRmtOn(v->voice, AX_PB_REMOTE_ON);
			info[chan].play = 1;//いろいろチェック用にスピーカの状態を表すフラグ
       	//--------------------------------------------------------------


        	AXSetVoiceState(v->voice, AX_PB_STATE_RUN);
            
        	
        }
    }
    
    OSRestoreInterrupts(old);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////


*/


/*---------------------------------------------------------------------------*
 * Name        : SpeakerCallback
 * Description : サンプル用のパラメータ初期化などはどうでもよい処理
 *　　　　　　　Speaker ONkからコールバックをはしごして最終的に初期化が完了するとここにくる
 * Returns     : None.
 *---------------------------------------------------------------------------*/
static void SpeakerCallback( s32 chan, s32 result )
{
    if (result == WPAD_ERR_NONE)
    {	//コントローラを管理するためのフラグなどもろもろ
        info[chan].Speakers.active = 1;
        info[chan].Speakers.first  = TRUE;
        info[chan].Speakers.last   = FALSE;
        memset(&info[chan].Speakers.encInfo, 0, sizeof(WENCInfo));


        OSReport("再生可能状態になりますた。　リモコンNo(%d)\n", chan);
    }
}

/*---------------------------------------------------------------------------*
 * Name        : SpeakerOnCallback
 * Description : 　　　スピーカーを_ONにしたら呼び出されるコールバック
 *　　　　　　　　　　　ONしたら次に_PLAYにする必要がある
 * Returns     : None.
 *---------------------------------------------------------------------------*/
void SpeakerOnCallback( s32 chan, s32 result )
{
    if (result == WPAD_ERR_NONE)
    {	//コントローラが再生可能になったら呼び出すコールバック登録
        OSReport("スピーカがONになりますた。　リモコンNo(%d)\n", chan);
        WPADControlSpeaker(chan, WPAD_SPEAKER_PLAY, SpeakerCallback);
    }
}

/*---------------------------------------------------------------------------*
 * Name        : SpeakerOffCallback
 * Description : 
 * Arguments   : 
 * Returns     : None.
 *---------------------------------------------------------------------------*/
static void SpeakerOffCallback( s32 chan, s32 result )
{
#pragma unused(result)

    info[chan].Speakers.active = 0;

    OSReport("スピーカがOFFになりますた。　リモコンNo(%d)\n", chan);
}

/*---------------------------------------------------------------------------*
 * Name        : ExtensionCallback
 * Description : 
 * Arguments   : 
 * Returns     : None.
 *---------------------------------------------------------------------------*/
/*static void ExtensionCallback( s32 chan, s32 result )
{
    switch(result)
    {
        case WPAD_DEV_CORE:
        case WPAD_DEV_FUTURE:
        case WPAD_DEV_NOT_SUPPORTED: WPADSetDataFormat(chan, WPAD_FMT_CORE);      break;
        case WPAD_DEV_FREESTYLE:     WPADSetDataFormat(chan, WPAD_FMT_FREESTYLE); break;
        case WPAD_DEV_CLASSIC:       WPADSetDataFormat(chan, WPAD_FMT_CLASSIC);   break;
    }
}
*/
/*---------------------------------------------------------------------------*
 * Name        : ConnectCallback
 * Description : リモコンが接続されると呼び出されるコールバック
 * Arguments   : 
 * Returns     : None.
 *---------------------------------------------------------------------------*/
static void ConnectCallback( s32 chan, s32 reason )
{

    OSReport("接続できますた　リモコンNo(%d)\n", chan);

    info[chan].Speakers.active = 0;
    if (reason >= 0)
    {
        WPADSetDataFormat(chan, WPAD_FMT_CORE);
    }
}

/*---------------------------------------------------------------------------*
 * Name        : myAlloc
 * Description : 
 * Arguments   : None.
 * Returns     : None.
 *---------------------------------------------------------------------------*/
/*static void *myAlloc( u32 size )
{
    void *ptr;

    ptr = MEMAllocFromExpHeap(hExpHeap, size);
    ASSERTMSG(ptr, "Memory allocation failed\n");

    return(ptr);
} 
*/
/*---------------------------------------------------------------------------*
 * Name        : myFree
 * Description : 
 * Arguments   : None.
 * Returns     : None.
 *---------------------------------------------------------------------------*/
/*static u8 myFree( void *ptr )
{
    MEMFreeToExpHeap(hExpHeap, ptr);
    return(1);
}
*/
/*---------------------------------------------------------------------------*
 * Name        : AudioFrameCallback
 * Description : Callback that process audio data per 3ms audio frame.
 * Arguments   : None.
 * Returns:    : None.
 *---------------------------------------------------------------------------*/
/*static void AudioFrameCallback( void )
{
    s32 i;

    info[0].play = info[1].play = info[2].play = info[3].play = 0;
    
    for (i = 0; i < AX_MAX_VOICES; i++)
    {
        if (vInfo[i].voice)
        {
            if ( AX_PB_STATE_STOP == ((vInfo[i].voice)->pb.state))
            {
                MIXReleaseChannel(vInfo[i].voice);
                AXFreeVoice(vInfo[i].voice);
                vInfo[i].voice = NULL;
                vInfo[i].entry = NULL;
                vInfo[i].chan  = -1;
            }
            else
            {
                info[vInfo[i].chan].play = 1;
            }
        }
    }
    
    for (i = 0; i < WPAD_MAX_CONTROLLERS; i++)
    {
        if (!info[i].play && info[i].Speakers.active)
        {
            WPADControlSpeaker(i, WPAD_SPEAKER_OFF, SpeakerOffCallback);
        }
    }
    
    // tell the mixer to update settings
    MIXUpdateSettings();
}
*/
/*---------------------------------------------------------------------------*
 * Name        : LoadFileIntoRam
 * Description : Loads a file into memory. Memory is allocated.
 * Arguments   : path    File to load into main memory
 * Returns:    : pointer to file in main memory or NULL if not opened
 *---------------------------------------------------------------------------*/
/*static void *LoadFileIntoRam( char *path )
{
    DVDFileInfo  handle;
    u32          round_length;
    s32          read_length;
    void        *buffer;

    // Open File
    if (!DVDOpen(path, &handle))
    {
        OSReport("WARNING! Failed to open %s\n", path);
        return NULL;
    }

    // Make sure file length is not 0
    if (DVDGetLength(&handle) == 0)
    {
        OSReport("WARNING! File length is 0\n");
        return NULL;
    }

    round_length = OSRoundUp32B(DVDGetLength(&handle));
    buffer       = MEMAllocFromExpHeapEx(hExpHeap, round_length,  32);

    // Make sure we got a buffer
    if (buffer == NULL)
    {
        OSReport("WARNING! Unable to allocate buffer\n");
        return NULL;
    }

    // Read Files
    read_length  = DVDRead(&handle, buffer, (s32)(round_length), 0);

    // Make sure we read the file correctly
    if (read_length <= 0)
    {
        OSReport("WARNING! File lenght is wrong\n");
        return NULL;
    }

    return buffer;
}
*/
/*---------------------------------------------------------------------------*
 * Name        : InitVoice
 * Description : 
 * Arguments   : None.
 * Returns:    : None.
 *---------------------------------------------------------------------------*/
/*static void InitVoice( void )
{
    s32 i;
    for (i = 0; i < AX_MAX_VOICES; i++)
    {
        vInfo[i].voice = NULL;
        vInfo[i].entry = NULL;
        vInfo[i].chan  = -1;
    }
}
*/

/*---------------------------------------------------------------------------*
 * Name        : StopSfx
 * Description : 
 * Arguments   : chan
 * Returns:    : None.
 *---------------------------------------------------------------------------*/
/*static void StopSfx( s32 chan )
{
    s32        i;
    BOOL       old;

    old = OSDisableInterrupts();

    for (i = 0; i < AX_MAX_VOICES; i++)
    {
        if (chan == vInfo[i].chan)
        {
            AXSetVoiceState(vInfo[i].voice, AX_PB_STATE_STOP);
        }
    }
    
    OSRestoreInterrupts(old);
}
*/
/*---------------------------------------------------------------------------*
 * Name        : GetVoice
 * Description : 
 * Arguments   : None.
 * Returns:    : pointer to VoiceInfo.
 *---------------------------------------------------------------------------*/
/*static VoiceInfo *GetVoice( void )
{
    s32 i;

    for (i = 0; i < AX_MAX_VOICES; i++)
    {
        if (NULL == vInfo[i].voice)
        {
            return(&vInfo[i]);
        }
    }
    
    return(NULL);
}
*/
/*---------------------------------------------------------------------------*
 * Name        : DropVoiceCallback
 * Description : 
 * Arguments   : 
 * Returns:    : None.
 *---------------------------------------------------------------------------*/
/*static void DropVoiceCallback( void *p )
{
    s32 i;

    for (i = 0; i < AX_MAX_VOICES; i++)
    {
        if  ( (AXVPB *)(p) == vInfo[i].voice)
        {
            MIXReleaseChannel(vInfo[i].voice);
            vInfo[i].voice = NULL;
            vInfo[i].entry = NULL;
            vInfo[i].chan  = -1;
            break;
        }
    }
}
*/
/*---------------------------------------------------------------------------*
 * Name        : RenderOperation
 * Description : 
 * Arguments   : None.
 * Returns:    : None.
 *---------------------------------------------------------------------------*/
/*static const s16 HEIGHT = 10;
static void RenderOperation( void )
{
    s16 y = 80;
    
    DEMOPrintf( 16, y += HEIGHT, 0, "button A : Start/Stop Sfx");
}
*/
/*---------------------------------------------------------------------------*
 * Name        : RenderControllerStatus
 * Description : 
 * Arguments   : None.
 * Returns:    : None.
 *---------------------------------------------------------------------------*/
/*static void RenderControllerStatus( void )
{
    s16 y = 32;
    int chan;

    DEMOPrintf( 150, y, 0, "speaker");
    DEMOPrintf( 220, y, 0, "sfx");
    for( chan = 0; chan < WPAD_MAX_CONTROLLERS; chan++)
    {
        y += HEIGHT;
        DEMOPrintf( 16, y, 0, "CHAN_%d [%s]", 
            chan, 
            (info[chan].status == WPAD_ERR_NO_CONTROLLER) ? "--" :
            (info[chan].type == 0) ? "CORE"     : 
            (info[chan].type == 1) ? "NUNCHAKU" : 
            (info[chan].type == 2) ? "CLASSIC"  :
                                     "UNKNOWN"
        );
        DEMOPrintf( 150, y, 0, "%s", (info[chan].Speakers.active == 1) ? "ON"   : 
                                     (info[chan].Speakers.active == 2) ? "MUTE" : 
                                                                         "OFF");
        DEMOPrintf( 220, y, 0, "%s", (info[chan].play == 0) ?            "STOP" :
                                                                         "PLAY");
    }
}
*/
/*---------------------------------------------------------------------------*
 * Name        : initialize
 * Description : Initialize GX.
 * Arguments   : None.
 * Returns:    : None.
 *---------------------------------------------------------------------------*/
/*static void initialize( void )
{
    const GXColor DARKBLUE      = { 0, 0, 64, 255 };
    const int     SCREEN_WIDTH  = 320;
    const int     SCREEN_HEIGHT = 240;

    DEMOInit( &GXNtsc480IntDf );
    GXSetCopyClear( DARKBLUE, GX_MAX_Z24 );
    GXCopyDisp( DEMOGetCurrentBuffer(), GX_TRUE );
    DEMOInitCaption( DM_FT_XLU, SCREEN_WIDTH, SCREEN_HEIGHT );
    GXSetZMode( GX_ENABLE, GX_ALWAYS, GX_ENABLE );                       // Set pixel processing mode
    GXSetBlendMode( GX_BM_BLEND, GX_BL_ONE, GX_BL_ONE, GX_LO_CLEAR );    // Translucent mode

    DEMOPadInit();
}
*/