#include <revolution.h>
#include <revolution/kpad.h>
#define DEMO_USE_MEMLIB
#include <demo.h>			//
#include <revolution/gx.h> 	// OSFatalを使用する場合includeが必要

#include "mem2allocator.h"
#include <math.h>
#include <stdio.h>			// sprintfなども使用できます
#include "sprite.h"
#include "Controller.h"

#include "main.h"
#include "Game.h"
#include "Timer.h"
#include "Number.h"

static int g_nMax = 0;
static int count = 90;
static int nCount = 60;
static bool nEnd;

void TimerInit(void)
{
	g_nMax = NumberInit(MAX_TIMER);
}

void TimerInit2(void)
{
	count = 60;
	nCount = 60;
	nEnd = false;
}

void TimerUninit(void)
{
	count = 0;
}

void TimerUpdate(void)
{
	if (count != 0)
	{
		if (nCount == 0)
		{
			nCount = 60;
			count--;
		}
		nCount--;
	}
	if (count == 0)
	{
		nEnd = true;
	}
}

void TimerDraw(GXTexObj* pTex)
{
	NumberSpriteSetColor(255, 0, 0, 255);
	NumberSpritrSetScale(0.5f, 0.5f);
	NumberTexSetSize(NUMBER_W, NUMBER_H, NUMBER_W, NUMBER_H);
	NumberSpritrSetSize(0.0f, 0.0f, NUMBER_W, NUMBER_H);
	NumberSetAnimasion(count, 1, NUM_NUMBER, NUM_NUMBER_W);
	NumberSetDraw(pTex);
	NumberDraw(MAX_TIMER, g_nMax, true, true, count);
}

int TimerGetFade(void)
{
	return nEnd;
}