//=======================================================================================
//
//		primitive.c[スプライト以外のポリゴン]
//    	AT12A242　(名前)	2017/05/19
//
//=======================================================================================
#include "main.h"
#include "primitive.h" 

//=======================================================================================
// 図形描画準備関数
// PrimitiveのDraw関数を呼ぶ前にコールする
// ※他のSetup関数が呼ばれた後、PrimitiveのDraw関数を呼ぶ場合には再度コールすること
void PrimitiveSetup(void)
{
	//ポリゴン頂点の定義
	GXClearVtxDesc();
	GXSetVtxDesc(GX_VA_POS, GX_DIRECT);		// 属性をクリア
	GXSetVtxDesc(GX_VA_CLR0, GX_DIRECT);	// 頂点データを直接Wiiへ１つずつセットする。
	GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_POS, GX_POS_XYZ, GX_F32, 0);	// 頂点属性の設定部分
	GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_CLR0, GX_CLR_RGB, GX_RGB8, 0);

	// 使用するTEVステージ数の設定
	GXSetNumTevStages(1); // 1つ

	// カラーチャンネル数の設定
	GXSetNumChans(1); // 1つ

	// 利用可能にするテクスチャ座標数の設定
	GXSetNumTexGens(1); // つかう

	// TEVで使用するテクスチャーカラーとラスタライズカラーの設定
	GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD_NULL, GX_TEX_DISABLE, GX_COLOR0A0);

	// TEVが上の要素のどのように合成するか？を設定する
	GXSetTevOp(GX_TEVSTAGE0, GX_PASSCLR);
}

//=======================================================================================
// 図形色設定関数
// 図形色を指定する
// この関数が呼ばれた後の図形はすべてここで指定した色になる
// r ... 赤要素(0-255)
// g ... 緑要素(0-255)
// b ... 青要素(0-255)
void PrimitiveSetColor(u8 r, u8 g, u8 b)
{
	primitiveR = r;
	primitiveG = g;
	primitiveB = b;
}

//=======================================================================================
// 点の描画
// x, y ... 点の描画座標
void PrimitiveDrawPoint(float x, float y)
{
	GXBegin(GX_POINTS, GX_VTXFMT0, 1);				//点の描画(1本分)
	GXPosition3f32(x, y, 0.0f);						//点の座標
	GXColor3u8(primitiveR, primitiveG, primitiveB);	//頂点色
	GXEnd();
}

//=======================================================================================
// 線の描画
// x0, y0 ... 線の始点座標
// x1, y1 ... 線の終点座標
void PrimitiveDrawLine(float x0, float y0, float x1, float y1)
{
	GXBegin(GX_LINES, GX_VTXFMT0, 2);				//線の描画(2本分)
	GXPosition3f32(x0, y0, 0.0f);					//線の始点
	GXColor3u8(primitiveR, primitiveG, primitiveB);	//頂点色
	GXPosition3f32(x1, y1, 0.0f);					//線の終点
	GXColor3u8(primitiveR, primitiveG, primitiveB);	//頂点色
	GXEnd();
}

//=======================================================================================
// 四角の描画
// x, y ... 四角の左上座標
// w, h ... 四角の幅と高さ
void PrimitiveDrawRectangle(float x, float y, float w, float h)
{
	GXBegin(GX_LINESTRIP, GX_VTXFMT0, 5);			//線の描画(4本分)
	
	GXPosition3f32(x, y, 0.0f);						//線の始点
	GXColor3u8(primitiveR, primitiveG, primitiveB);	//頂点色
	
	GXPosition3f32((x + w), y, 0.0f);				//線の終点
	GXColor3u8(primitiveR, primitiveG, primitiveB);	//頂点色
	
	GXPosition3f32((x + w), (y + h), 0.0f);			//線の終点
	GXColor3u8(primitiveR, primitiveG, primitiveB);	//頂点色
		
	GXPosition3f32(x, (y + h), 0.0f);				//線の始点	
	GXColor3u8(primitiveR, primitiveG, primitiveB);	//頂点色	
	
	GXPosition3f32(x, y, 0.0f);						//線の始点
	GXColor3u8(primitiveR, primitiveG, primitiveB);	//頂点色
	GXEnd();
}

//=======================================================================================
// 塗りつぶし四角の描画
// x, y ... 四角の左上座標
// w, h ... 四角の幅と高さ
void PrimitiveDrawFillRectangle(float x, float y, float w, float h)
{
	GXBegin(GX_TRIANGLESTRIP, GX_VTXFMT0, 4);
	GXPosition3f32(x, y, 0.0f);						//頂点0
	GXColor3u8(primitiveR, primitiveG, primitiveB);	//頂点色
	GXPosition3f32((x + w), y, 0.0f);				//頂点1
	GXColor3u8(primitiveR, primitiveG, primitiveB);	//頂点色
	GXPosition3f32(x, (y + h), 0.0f);				//頂点3
	GXColor3u8(primitiveR, primitiveG, primitiveB);	//頂点色
	GXPosition3f32((x + w), (y + h), 0.0f);			//頂点2
	GXColor3u8(primitiveR, primitiveG, primitiveB);	//頂点色
	GXEnd();
}

//=======================================================================================
// 塗りつぶされた多角形の描画
// 座標をいくつも登録してその座標を頂点とする多角形を描画する
// v   ... 座標配列の先頭アドレス
// num ... 座標の数
void PrimitiveDrawPolygon(Vec2 v[], u16 num)
{
	int i;

	GXBegin(GX_TRIANGLEFAN, GX_VTXFMT0, num);
	for (i = 0; i<num; i++)
	{
		GXPosition3f32(v[i].x, v[i].y, 0.0f);			//頂点0
		GXColor3u8(primitiveR, primitiveG, primitiveB);	//頂点色
	}
	GXEnd();
}

//=======================================================================================
// ラインの太さ
// nLineSize ... ラインの太さ
void LineSize(int nLineSize)
{
	GXSetLineWidth(nLineSize, GX_TO_ZERO);		//ラインの太さ
}

//=======================================================================================
// ラインの太さ
// nPointSize ... 点の太さ
void PointSize(int nPointSize)
{
	GXSetPointSize(nPointSize, GX_TO_ZERO);		//点のサイズ	
}