#ifndef _RECTELE_H_
#define _RECTELE_H_

typedef enum
{
	TEXTURE_NUM_RECTELE = 0,
	TEXTURE_RECTELE_MAX
}TEXTURE_RECTELE_DATA;

void RecteleCase(void);
void RecteleInit(void);
void RecteleUninit(void);
void RecteleUpdate(void);
void RecteleDraw(void);

#endif // !_RECTELE_H_
