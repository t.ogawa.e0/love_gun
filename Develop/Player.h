#ifndef _PLAYER_H_
#define _PLAYER_H_

void PlayerInit(void);
void PlayerUninit(void);
int PlayerUpdate(void);
void PlayerDraw(void);

#endif // !_PLAYER_H_