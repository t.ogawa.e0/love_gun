#include <revolution.h>
#include <revolution/kpad.h>
#define DEMO_USE_MEMLIB
#include <demo.h>			//
#include <revolution/gx.h> 	// OSFatalを使用する場合includeが必要

#include "mem2allocator.h"
#include <math.h>
#include <stdio.h>			// sprintfなども使用できます
#include <stdlib.h>
#include "main.h"
#include "Game.h"
#include "sprite.h"
#include "Controller.h"
#include "Timer.h"
#include "Number.h"
#include "Enemy.h"
#include "Bullet.h"
#include "Background.h"
#include "Score.h"

typedef struct _LOVEFEF
{
	float PosX, PosY, PosW, PosH;
	float PosPulus;
	bool bRelese;
	int LoveCount;
}LOVEFEF;
static TPLPalettePtr texPal;				//テクスチャパレットを表示する型の変数
static GXTexObj GameTexObj[TEXTURE_GAME_MAX];	//テクスチャオブジェクトを保存する配列
static bool g_fade;
static LOVEFEF g_aLove[NUMLOVE];

void GameCase(void)
{
	GameUpdate();
	GameDraw();
}
int GameInit2(void)
{
	u16 TexWiz;	//横サイズ
	u16 TexHei;	//縦サイズ
	int nCount;
	int nTexNum = 0;
	int nID = 0;
	int nNotTexNum = 2555;
	int nTexNumBer[2] =
	{
		TEXTURE_mens1_5,
		TEXTURE_girl1_4,
	};

	TexWiz = GXGetTexObjWidth(&GameTexObj[TEXTURE_Love]);	//横サイズ
	TexHei = GXGetTexObjHeight(&GameTexObj[TEXTURE_Love]);	//縦サイズ

	for (nCount = 0; nCount < NUMLOVE; nCount++)
	{

		g_aLove[nCount].PosX = g_aLove[nCount].PosY = 0.0f;
		g_aLove[nCount].PosW = TexWiz / 8;
		g_aLove[nCount].PosH = TexHei / 8;
		g_aLove[nCount].LoveCount = 0;
		g_aLove[nCount].bRelese = false;
		g_aLove[nCount].PosPulus = 0;
	}


	for (nCount = 0; nCount < MAX_ENEMY; nCount++)
	{
		for (;;)
		{
			nTexNum = rand() % (int)(sizeof(nTexNumBer) / 4);
			if (nNotTexNum != nTexNum)
			{
				nNotTexNum = nTexNum;
				break;
			}
		}
		//		if (0 <= nTexNum&&nTexNum <= 3) { nID = 0; }
		//		if (4 <= nTexNum&&nTexNum <= 7) { nID = 1; }
		EnemyInit(&GameTexObj[nTexNumBer[nTexNum]], nCount, nTexNum + 1, nTexNumBer[nTexNum]);
	}
	TimerInit2();
	g_fade = false;
}
void GameInit(void)
{
	int nCount;

	// TPLファイルノロード
	TPLGetPalette(&texPal, "game.tpl");

	//ロードエラーチェック
	ASSERTMSG(TEXTURE_GAME_MAX == texPal->numDescriptors, "\nTPL内画像総数とNUM_MAXの値が一致していない\n(numDescriptors==%d)!=(NUM_MAX=%d)\n", texPal->numDescriptors, TEXTURE_GAME_MAX);

	//TPLからテクスチャオブジェクトを取り出す
	for (nCount = 0; nCount < texPal->numDescriptors; nCount++)
	{
		TPLGetGXTexObjFromPalette(texPal, &GameTexObj[nCount], nCount);
	}
	TimerInit();
	GameInit2();
	BulletInit();
	BackgroundInit();
	GameInit2();
}
void GameUninit(void)
{
	TimerUninit();
	EnemyUninit();
	BulletUninit();
	BackgroundUninit();
	
	// テクスチャパレットを解放
	TPLReleasePalette(&texPal);
}
void GameUpdate(void)
{
	TimerUpdate();
	g_fade=TimerGetFade();
	EnemyUpdate();
	BulletUpdate();
	BackgroundUpdate();
	LoveUpdate();
}
void GameDraw(void)
{
	int nCount = 0;

	// 背景
	BackgroundDraw(&GameTexObj[TEXTURE_NUM_GAME]);

	for (nCount = 0; nCount < MAX_ENEMY; nCount++)
	{
		EnemyDraw(&GameTexObj[EnemyGetTexNunBer(nCount)], nCount);
	}

	//	BulletDraw();
	TimerDraw(&GameTexObj[TEXTURE_NUMBER]);
	ScorTexSet(&GameTexObj[TEXTURE_NUMBER]);
	LoveDraw();
}

int GameGetFade(void)
{
	return g_fade;
}

void LoveSet(float PosX,float PosY)
{
	int nCount;
	
	for(nCount=0;nCount<NUMLOVE;nCount++)
	{
		if(g_aLove[nCount].bRelese==false)
		{
			g_aLove[nCount].PosX=PosX;
			g_aLove[nCount].PosY=PosY;
			g_aLove[nCount].bRelese=true;
			break;
		}
	}
}

void LoveUpdate(void)
{
	int nCount;
	
	for(nCount=0;nCount<NUMLOVE;nCount++)
	{
		if(g_aLove[nCount].bRelese==true)
		{
			g_aLove[nCount].PosY-=0.2;
			g_aLove[nCount].PosX;
			g_aLove[nCount].LoveCount++;
			if(g_aLove[nCount].LoveCount==30)
			{
				g_aLove[nCount].LoveCount=0;
				g_aLove[nCount].PosPulus=0;
				g_aLove[nCount].bRelese=false;
			}
		}
	}
}
void LoveDraw(void)
{
	int nCount;
	u16 TexWiz;	//横サイズ
	u16 TexHei;	//縦サイズ
	
	TexWiz = GXGetTexObjWidth(&GameTexObj[TEXTURE_Love]);	//横サイズ
	TexHei = GXGetTexObjHeight(&GameTexObj[TEXTURE_Love]);	//縦サイズ
	for(nCount=0;nCount<NUMLOVE;nCount++)
	{
		if(g_aLove[nCount].bRelese==true)
		{
			SpriteSetup();
			SpriteSetColor(255, 255, 255, 255);
			SpritrSetScale(1.0f,1.0f);	
			TexSetSize(0,0,TexWiz, TexHei);
			SpritrSetSize(g_aLove[nCount].PosX,g_aLove[nCount].PosY,g_aLove[nCount].PosW, g_aLove[nCount].PosH);
			SpriteDraw(&GameTexObj[TEXTURE_Love]);
		}
	}
}