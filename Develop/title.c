#include <revolution.h>
#include <revolution/kpad.h>
#define DEMO_USE_MEMLIB
#include <demo.h>			//
#include "mem2allocator.h"

#include <math.h>
#include <revolution/gx.h> 	// OSFatalを使用する場合includeが必要
#include <stdio.h>			// sprintfなども使用できます

#include "main.h"
#include "title.h"
#include "sprite.h"
#include "Controller.h"

static TPLPalettePtr texPal;						//テクスチャパレットを表示する型の変数
static GXTexObj TaitleTexObj[TEXTURE_TITLE_MAX];	//テクスチャオブジェクトを保存する配列

void TitleCase(void)
{
	TitleUpdate();
	TitleDraw();
}
void TitleInit(void)
{
	int nCount;

	// TPLファイルノロード
	TPLGetPalette(&texPal, "title.tpl");

	//ロードエラーチェック
	ASSERTMSG(TEXTURE_TITLE_MAX == texPal->numDescriptors, "\nTPL内画像総数とNUM_MAXの値が一致していない\n(numDescriptors==%d)!=(NUM_MAX=%d)\n", texPal->numDescriptors, TEXTURE_TITLE_MAX);

	//TPLからテクスチャオブジェクトを取り出す
	for (nCount = 0; nCount < texPal->numDescriptors; nCount++)
	{
		TPLGetGXTexObjFromPalette(texPal, &TaitleTexObj[nCount], nCount);
	}
	
	
}
void TitleUninit(void)
{
	// テクスチャパレットを解放
	TPLReleasePalette(&texPal);
}
void TitleUpdate(void)
{

}
void TitleDraw(void)
{
	SpriteSetup();
	SpriteSetColor(255, 255, 255, 255);
	SpritrSetScale(1.0f, 1.0f);
	TexSetSize(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	SpritrSetSize(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	SpriteDraw(&TaitleTexObj[TEXTURE_NUM_TITLE]);
}