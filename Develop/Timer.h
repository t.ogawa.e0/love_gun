#ifndef _TIMER_H_
#define _TIMER_H_

#define MAX_TIMER (3)

void TimerInit(void);
void TimerInit2(void);
void TimerUninit(void);
void TimerUpdate(void);
void TimerDraw(GXTexObj* pTex);
int TimerGetFade(void);

#endif // !_TIMER_H_
