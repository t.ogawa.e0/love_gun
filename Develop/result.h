#ifndef _RESULT_H_
#define _RESULT_H_

typedef enum
{
	TEXTURE_RESULT_MAN = 0,
	TEXTURE_RESULT_GAL,
	TEXTURE_RESULT_MAX
}TEXTURE_RESULT_DATA;

void ResultCase(void);
void ResultInit(void);
void ResultUninit(void);
void ResultUpdate(void);
void ResultDraw(void);

#endif // !_RESULT_H_
