//=======================================================================================
//
//=======================================================================================


//----- インクルードファイル -----

#include <revolution.h>
#include <revolution/sp.h>
#include <revolution/mix.h>
#include <string.h>
#include <math.h>

#include "YSound.h"


//----- 構造体定義 -----

// ストリーム情報構造体
typedef struct
{
    DVDFileInfo fileInfoL; // Lチャンネルファイル情報
    DVDFileInfo fileInfoR; // Rチャンネルファイル情報
    
    s32 done;
    s32 rest;       // ストリーム(バッファ)の残り
    s32 offset;     // ストリーム(バッファ)のオフセット(書き込み位置)
    u32 end_addr;   // 終了アドレス
	
} StreamInfo;


//----- グローバル変数 -----

// ストリーム情報管理変数
static StreamInfo _streamInfo = {0};

// ストリームサウンドボリューム
static s32 _stream_dB = 0;
static s32 _streamVolume = 100;

// オンメモリサウンドボリューム
static s32 _onMemory_dB = 0;
static s32 _onMemoryVolume = 100;

// AX VOICE パラメータブロック
static AXVPB* _axvpbStreamL = NULL;
static AXVPB* _axvpbStreamR = NULL;

// DVD制御フラグ
static BOOL _bDVDActive = FALSE;

// 書き込みバッファのオフセット
static s32 _writeBufferOffset = 0;

// ストリームバッファ
#define _STREAMBUFFER_SIZE (32 * 1024 * 2) // 32KB x DoubleBuffer (byte)
static u8* _streamBufferL = NULL;
static u8* _streamBufferR = NULL;

// ストリーム位置
static u32 _halfPos = 0;
static u32 _prevPos = 0;

// ストリーム状態管理変数
static YSoundStreamState _streamState = YSOUND_STREAM_STATE_NONE;        


// オンメモリサウンド情報
static SPSoundTable* _spTable;

// オンメモリサウンドデータ
static u8* _spData;

// オンメモリ管理構造体
typedef struct 
{
    // AX VOICE パラメータブロック
    AXVPB *axvpb;
    // サウンドテーブル情報
    SPSoundEntry *spSoundEntry;
    // ポーズフラグ
    BOOL pause;

} OnMemoryInfo;

// オンメモリで使える最大Voice数
#define ONMEMORY_MAX_VOICES (AX_MAX_VOICES - 2)
// ※ストリーム２チャンネル分を考慮

// オンメモリ管理リスト
static OnMemoryInfo _onMemoryInfo[ONMEMORY_MAX_VOICES]; 


// YSoundSystem用アロケータ
static YSoundAlloc _alloc = NULL;
static YSoundFree _free = NULL;

// SPSoundEntry構造体のメンバ 'type' をチェックしてループVoiceか否かをチェックする
static inline BOOL _isLooped(SPSoundEntry* p)
{
    return (p->type & 0x1) != 0;
}

// ファイルをメモリに読み込む
static void* _loadFileIntoMemory(const char* fileName)
{
    void* buffer;
    DVDFileInfo handle;
    u32 round_length;
    s32 read_length;

    if( _alloc == NULL ) {
    
        OSHalt("YSoundが初期化されていない状態で処理が呼ばれました...\n");
    }
    
    // ファイルオープン
    if( !DVDOpen(fileName, &handle) ) {
    
        OSReport("ファイル(%s)が開けませんでした...\n", fileName);
        return NULL;
    }

    // ファイルサイズチェック
    if( DVDGetLength(&handle) == 0 ) {
    
        OSReport("ファイル(%s)サイズが0です...\n", fileName);
        return NULL;
    }

    // 確保するメモリ量をファイルサイズを32Byteで割り切れるサイズにする
    round_length = OSRoundUp32B(DVDGetLength(&handle));
    
    // メモリ確保
    buffer = _alloc(round_length);

    // 確保できたか？
    if( buffer == NULL )
    {
        OSReport("ファイル(%s)分のメモリの確保に失敗しました...\n", fileName);
    }

    // ファイルの読み込み
    read_length = DVDRead(&handle, buffer, (s32)(round_length), 0);

    // 正しく読み込めたか？
    if( read_length <= 0 )
    {
        OSReport("正しくファイル(%s)を読み込めませんでした...\n", fileName);
        return NULL;
    }
    
    return buffer;
}


// DVDの読み込み終了時に呼び出される関数
// ※読み込み状況を監視してAXPBADPCMLOOPパラメータを設定する
static void _readEndStreamData(s32 result, DVDFileInfo* fileInfo)
{
// 引数は今回未使用
#pragma unused(result)
#pragma unused(fileInfo)

    // バッファの最初の半分が埋まったら
    if( _writeBufferOffset == 0 && _streamState == YSOUND_STREAM_STATE_STARTED )
    {
        AXPBADPCMLOOP loop;        

        loop.loop_pred_scale = (u16)(*((u8*)(_streamBufferR)));
        // loop_yn1 と loop_yn2には設定しない

        // 右側のAXPBADPCMLOOPパラメータを設定
        AXSetVoiceAdpcmLoop(_axvpbStreamR, &loop);
    }
    
    // DVD使い終わったよ〜
    _bDVDActive = FALSE;
}


// 右チャンネルデータのストリーム読み込み
static void _streamReadDataR(s32 result, DVDFileInfo* fileInfo)
{
// 引数は今回未使用
#pragma unused(result)
#pragma unused(fileInfo)
    
    // 読み込みサイズの算出
    s32 size = _STREAMBUFFER_SIZE >> 1; // ストリームバッファサイズの半分
 
    // 書き込み先バッファアドレス算出
    u8* buffer = _streamBufferR + _writeBufferOffset;
    
    // 読み込むサイズの決定
    s32 length = _streamInfo.rest > size ? size : _streamInfo.rest;
    
    // 読み込むサイズを32Byteのアライメントに整える
    s32 rlength = length & ~0x1f; // 32バイトに丸める

    // DVDからの非同期読み込み
    // ※読み込み終わったら読み込み終了関数をコールバックする
    DVDReadAsync(&_streamInfo.fileInfoR, buffer, rlength, _streamInfo.offset, _readEndStreamData);
    
    // ストリームの残量とオフセット(読み込み位置)を更新
    _streamInfo.rest   -= rlength;
    _streamInfo.offset += rlength;

    // バッファの最初の半分が埋まったら
    if( _writeBufferOffset == 0 && _streamState == YSOUND_STREAM_STATE_STARTED )
    {
        AXPBADPCMLOOP loop;

        loop.loop_pred_scale = (u16)(*((u8*)(_streamBufferL)));
        // loop_yn1 と loop_yn2には設定しない

        // 左側のAXPBADPCMLOOPパラメータを設定
        AXSetVoiceAdpcmLoop(_axvpbStreamL, &loop);
    }
}


// 右チャンネルデータのストリーム読み込み
static void _streamReadDataL(void)
{
    // 読み込みサイズの算出
    s32 size = _STREAMBUFFER_SIZE >> 1; // ストリームバッファサイズの半分
 
    // 書き込み先バッファアドレス算出
    u8* buffer = _streamBufferL + _writeBufferOffset;
    
    // 読み込むサイズの決定
    s32 length = _streamInfo.rest > size ? size : _streamInfo.rest;
    
    // 読み込むサイズを32Byteのアライメントに整える
    s32 rlength = length & ~0x1f; // 32バイトに丸める

    // DVD使うよ〜
    _bDVDActive = TRUE;
    
    // DVDからの非同期読み込み
    // 読み込み終わったら右チャンネルのストリーム読み込み関数をコールバックする
    DVDReadAsync(&_streamInfo.fileInfoL, buffer, rlength, _streamInfo.offset, _streamReadDataR);
    
    // ストリーム(バッファ)の終端まで全部書き込み終わった...
    if( ((_streamInfo.rest - rlength) & ~0x1f) == 0 ) {

        _streamInfo.done++;
        
        if( rlength != 0 ) {
            // 書き込み終了アドレス(キャッシュでなくて物理)の保存
            _streamInfo.end_addr = (u32)OSCachedToPhysical(buffer) + rlength;
        }
    }
}

static void _onMemoryUpdate(void)
{
	int i;
	
    // 管理リストを全チェック
    for( i = 0; i < ONMEMORY_MAX_VOICES; i++ ) {

        // 使用中の情報を検索
        if( _onMemoryInfo[i].axvpb != NULL ) {
        
            // voiceの状態を確認して停止していたら
            if( ( _onMemoryInfo[i].axvpb->pb.state == AX_PB_STATE_STOP ) && ( !_onMemoryInfo[i].pause ) ) {

                // voiceの破棄処理をする
                MIXReleaseChannel(_onMemoryInfo[i].axvpb);
                AXFreeVoice(_onMemoryInfo[i].axvpb);

                // 管理リストの情報を空状態にする
                _onMemoryInfo[i].axvpb = NULL;
            }
        }
    }
}

// AX更新時に呼ばれる関数
static void _callbackUpdate(void)
{
    u32 currPos = 0;

    // ミキサーの設定を更新する
    MIXUpdateSettings();
    
    // オンメモリサウンドの更新
    _onMemoryUpdate();

    // ストリームの更新
    // ※状態をチェックしながら更新していくのが基本
        
    // ストリームの状態から処理を選択
    switch( _streamState )
    {
    case YSOUND_STREAM_STATE_NONE:
    case YSOUND_STREAM_STATE_INITIALIZING:
        break;

    case YSOUND_STREAM_STATE_STARTED:
    
        currPos = (u32)(_axvpbStreamL->pb.addr.currentAddressHi << 16) | (_axvpbStreamL->pb.addr.currentAddressLo);
        currPos >>= 1; // 半分
        
        if( _streamInfo.done ) {
        
            if( ((currPos < _prevPos) && ((_prevPos < _streamInfo.end_addr) || (_streamInfo.end_addr <= currPos))) ||
                ((_prevPos < currPos) && ((_prevPos < _streamInfo.end_addr) && (_streamInfo.end_addr <= currPos))) )
            {
                _streamState = YSOUND_STREAM_STATE_STOPPING;
            }
        }
        
        if( currPos < _prevPos ) {
        
            if( !_bDVDActive ) {
                _writeBufferOffset = _STREAMBUFFER_SIZE >> 1;
                _streamReadDataL();
                _prevPos = currPos;
            }
        }
        else if( (_prevPos < _halfPos) && (currPos >= _halfPos) )
        {

            if( !_bDVDActive )
            {
                _writeBufferOffset = 0;
                _streamReadDataL();
                _prevPos = currPos;
            }
        }
        else
        {
            _prevPos = currPos;
        }
        
        break;

    case YSOUND_STREAM_STATE_STOPPING:

        // 左チャンネル
        MIXReleaseChannel(_axvpbStreamL);
        AXFreeVoice(_axvpbStreamL);
        _axvpbStreamL = NULL;

        // 右チャンネル
        MIXReleaseChannel(_axvpbStreamR);
        AXFreeVoice(_axvpbStreamR);
        _axvpbStreamR = NULL;
        
        _streamState = YSOUND_STREAM_STATE_STOPPED;
        
        break;
        
    case YSOUND_STREAM_STATE_STOPPED:

        if( !_bDVDActive ) {
        
            DVDClose(&_streamInfo.fileInfoL);
            DVDClose(&_streamInfo.fileInfoR);
            _streamState = YSOUND_STREAM_STATE_NONE;
        }
        
        break;
    }
}

// オンメモリ管理リストの初期化
static void _initOnMemoryInfo(void)
{
	int i;
	
    for( i = 0; i < ONMEMORY_MAX_VOICES; i++ ) {
    
        _onMemoryInfo[i].axvpb = NULL;
        _onMemoryInfo[i].spSoundEntry = NULL;
        _onMemoryInfo[i].pause = FALSE;
    }
}

// 空きオンメモリ管理情報インデックスの取得
static int _getOnMemoryInfoIndex(void)
{
	int i;
	
    for(i = 0; i < ONMEMORY_MAX_VOICES; i++ ) {
    
        if( _onMemoryInfo[i].axvpb == NULL ) {
        
            return i;
        }
    } 
    
    // 空きがなかった...
    return -1;
}

// voice破棄時に呼ばれる関数
static void _callbackDropVoice(void *p)
{
	int i;
	
    // 破棄されたvoiceを管理リストから検索
    for( i = 0; i < ONMEMORY_MAX_VOICES; i++ ) {
    
        // 破棄voiceを管理しているデータを検索
        if( (AXVPB*)p == _onMemoryInfo[i].axvpb )
        {
            // ミキサーチャンネルから解放
            MIXReleaseChannel(_onMemoryInfo[i].axvpb);
            
            // 管理リストの情報を空状態にする
            _onMemoryInfo[i].axvpb = NULL;
            _onMemoryInfo[i].spSoundEntry = NULL;
            
            break;
        }
    }
}


void YSound_Init(YSoundAlloc alloc, YSoundFree free)
{
    // AIライブラリの初期化
    AIInit(NULL);
    // AISetDSPSampleRate(AI_SAMPLERATE_48KHZ);
    
    // AXライブラリ用メモリの確保
    AXInitSpecifyMem(AX_MAX_VOICES, alloc(AXGetMemorySize(AX_MAX_VOICES)));
    
    // MIXライブラリ用メモリの確保
    MIXInitSpecifyMem(alloc(MIXGetMemorySize(AX_MAX_VOICES)));
    
    // ストリーム用バッファの確保
    _streamBufferL = (u8*)alloc(_STREAMBUFFER_SIZE);
    _streamBufferR = (u8*)alloc(_STREAMBUFFER_SIZE);

    // AXの更新を通知するコールバック関数の登録
    AXRegisterCallback(&_callbackUpdate); 
    
    // アロケータの保存
    _alloc = alloc;
    _free = free;
}

void YSound_Uninit(void)
{
	MIXQuit();
    AXQuit();
}

YSoundStreamState YSound_GetStreamState(void)
{
    return _streamState;
}

void YSound_StartStream(const char* fileNameL, const char* fileNameR)
{
    s32        length;
    s32        rlength;
    DSPADPCM  *dspHeader;
    AXPBADDR   addr;
    AXPBADPCM  adpcm;
    AXPBSRC    src;
    u32        ls_addr;
    u32        le_addr;
    u32        cu_addr;
    u32 	   srcBits;
    
    int i;
    

    if( _streamState != YSOUND_STREAM_STATE_NONE ) {

        return;
    }

    _streamState = YSOUND_STREAM_STATE_INITIALIZING;
    
    // 左チャンネルのADPCMを開く
    if( !DVDOpen(fileNameL, &_streamInfo.fileInfoL) ) {

        OSHalt("Cannot open stream file L\n");
    }

    // 右チャンネルのADPCMを開く
    if( !DVDOpen(fileNameR, &_streamInfo.fileInfoR) ) {

        OSHalt("Cannot open stream file R\n");
    }
    
    _streamInfo.done   = 0;
    _streamInfo.rest   = (s32)DVDGetLength(&_streamInfo.fileInfoL);
    _streamInfo.offset = 0;

    // データの読み込み
    length  = _streamInfo.rest > _STREAMBUFFER_SIZE ? _STREAMBUFFER_SIZE : _streamInfo.rest;
    rlength = length & ~0x1f; // 32Byteに丸める
 
    DVDRead(&_streamInfo.fileInfoL, _streamBufferL, rlength, _streamInfo.offset);
    DVDRead(&_streamInfo.fileInfoR, _streamBufferR, rlength, _streamInfo.offset);
    
    _streamInfo.rest   -= rlength;
    _streamInfo.offset += rlength;

    if( !(_streamInfo.rest & ~0x1f) )
    {
        _streamInfo.done++;
        _streamInfo.end_addr = (u32)OSCachedToPhysical(_streamBufferL) + rlength;
    }
    
    // 読み込み位置の設定
    _halfPos = (u32)OSCachedToPhysical(_streamBufferL) + (_STREAMBUFFER_SIZE >> 1);
    _prevPos = (u32)OSCachedToPhysical(_streamBufferL);
            
    // ボイスの取得
    if( !(_axvpbStreamL = AXAcquireVoice(AX_PRIORITY_NODROP, NULL, 0)) || !(_axvpbStreamR = AXAcquireVoice(AX_PRIORITY_NODROP, NULL, 0)) ) {

        OSHalt("Cannot acquire voice\n");
    }
    	
    // 左チャンネルの設定
    dspHeader = (DSPADPCM*)_streamBufferL;
    
    ls_addr = (u32)OSCachedToPhysical(_streamBufferL) << 1;
    le_addr = ls_addr + (_STREAMBUFFER_SIZE << 1) - 1;
    cu_addr = ls_addr + (sizeof(DSPADPCM) << 1) + dspHeader->ca;
    
    ls_addr += 2; // フレームヘッダーを読み飛ばす
    
    // AXボイスとミキサーチャンネルを関連付け
    // ミキサーチャンネルの各パラメータを初期化
    // ボイスへのポインタ
    // Aux A/Aux B/Aux Cのモード、及びミュート設定
    // input コントロールの初期減衰を 0.1 dB 単位で指定します (1 = 0.1 dB) 
    // AuxA  コントロールの初期減衰を 0.1 dB 単位で指定します (1 = 0.1 dB) 
    // AuxB  コントロールの初期減衰を 0.1 dB 単位で指定します (1 = 0.1 dB) 
    // AuxC  コントロールの初期減衰を 0.1 dB 単位で指定します (1 = 0.1 dB)
    //       ※サウンドモードがドルビープロロジックII (MIX_SOUND_MODE_DPL2) の場合、ここの値は無視 
    // pan   コントロールの初期値を指定します (0 = 左, 64 = 中央, 127 = 右)
    // span  コントロールの初期値を指定します (0 = サラウンド, 64 = 中央, 127 = 前方)
    // fader コントロールの初期減衰を 0.1 dB 単位で指定します (1 = 0.1 dB)
    MIXInitChannel(_axvpbStreamL, 0, 0, -904, -904, -904, 0, 127, _stream_dB);

    addr.loopFlag         = AXPBADDR_LOOP_ON;
    addr.format           = AX_PB_FORMAT_ADPCM;
    addr.loopAddressHi    = (u16)(ls_addr >> 16);
    addr.loopAddressLo    = (u16)(ls_addr &  0xFFFF);
    addr.endAddressHi     = (u16)(le_addr >> 16);
    addr.endAddressLo     = (u16)(le_addr &  0xFFFF);
    addr.currentAddressHi = (u16)(cu_addr >> 16);
    addr.currentAddressLo = (u16)(cu_addr &  0xFFFF);
    
    for( i = 0; i < 16; i++ ) {     
        adpcm.a[i/2][i%2] = dspHeader->coef[i];
    }
    adpcm.gain            = dspHeader->gain; 
    adpcm.pred_scale      = dspHeader->ps;
    adpcm.yn1             = dspHeader->yn1;
    adpcm.yn2             = dspHeader->yn2;

    // AXPBSRC構造体の設定(サンプリングレート設定のための計算)
    srcBits = (u32)(0x00010000 * ((f32)dspHeader->sample_rate / AX_IN_SAMPLES_PER_SEC));
    src.ratioHi = (u16)(srcBits >> 16);
    src.ratioLo = (u16)(srcBits & 0xFFFF);
    src.currentAddressFrac = 0;
    src.last_samples[0] = 0;
    src.last_samples[1] = 0;
    src.last_samples[2] = 0;
    src.last_samples[3] = 0;  
    
    AXSetVoiceType   (_axvpbStreamL, AX_PB_TYPE_STREAM); // ADPCM データをストリーミング再生するタイプ
    AXSetVoiceAdpcm  (_axvpbStreamL, &adpcm);
    AXSetVoiceAddr   (_axvpbStreamL, &addr);
    AXSetVoiceSrcType(_axvpbStreamL, AX_SRC_TYPE_NONE);
    AXSetVoiceState  (_axvpbStreamL, AX_PB_STATE_RUN);
    AXSetVoiceSrcType(_axvpbStreamL, AX_SRC_TYPE_LINEAR);
    AXSetVoiceSrc    (_axvpbStreamL, &src);

    // 左チャンネルの設定
    dspHeader = (DSPADPCM *)_streamBufferR;
    
    ls_addr = (u32)OSCachedToPhysical(_streamBufferR) << 1;
    le_addr = ls_addr + (_STREAMBUFFER_SIZE << 1) - 1;
    cu_addr = ls_addr + (sizeof(DSPADPCM) << 1) + dspHeader->ca;
    
    ls_addr += 2; // フレームヘッダーを読み飛ばす
    
    // AXボイスとミキサーチャンネルを関連付け
    MIXInitChannel(_axvpbStreamR, 0, 0, -904, -904, -904, 127, 127, _stream_dB);

    addr.loopFlag         = AXPBADDR_LOOP_ON;
    addr.format           = AX_PB_FORMAT_ADPCM;
    addr.loopAddressHi    = (u16)(ls_addr >> 16);
    addr.loopAddressLo    = (u16)(ls_addr &  0xFFFF);
    addr.endAddressHi     = (u16)(le_addr >> 16);
    addr.endAddressLo     = (u16)(le_addr &  0xFFFF);
    addr.currentAddressHi = (u16)(cu_addr >> 16);
    addr.currentAddressLo = (u16)(cu_addr &  0xFFFF);
    
    for( i = 0; i < 16; i++ ) {     
        adpcm.a[i/2][i%2] = dspHeader->coef[i];
    }
    adpcm.gain            = dspHeader->gain; 
    adpcm.pred_scale      = dspHeader->ps;
    adpcm.yn1             = dspHeader->yn1;
    adpcm.yn2             = dspHeader->yn2;
    
    // AXPBSRC構造体の設定(サンプリングレート設定のための計算)
    srcBits = (u32)(0x00010000 * ((f32)dspHeader->sample_rate / AX_IN_SAMPLES_PER_SEC));
    src.ratioHi = (u16)(srcBits >> 16);
    src.ratioLo = (u16)(srcBits & 0xFFFF);
    src.currentAddressFrac = 0;
    src.last_samples[0] = 0;
    src.last_samples[1] = 0;
    src.last_samples[2] = 0;
    src.last_samples[3] = 0;      
    
    AXSetVoiceType   (_axvpbStreamR, AX_PB_TYPE_STREAM); // ADPCM データをストリーミング再生するタイプ
    AXSetVoiceAdpcm  (_axvpbStreamR, &adpcm);
    AXSetVoiceAddr   (_axvpbStreamR, &addr);
    AXSetVoiceSrcType(_axvpbStreamR, AX_SRC_TYPE_NONE);
    AXSetVoiceState  (_axvpbStreamR, AX_PB_STATE_RUN);
    AXSetVoiceSrcType(_axvpbStreamR, AX_SRC_TYPE_LINEAR);
    AXSetVoiceSrc    (_axvpbStreamR, &src);

    _streamState = YSOUND_STREAM_STATE_STARTED;
}

void YSound_SetStreamVolume(s32 volume) 
{
    // volumeは0〜100の間
	if( volume <= 0 ) {
	
	    _streamVolume = 0;
	    _stream_dB = -960;
	}
	else if( volume >= 100 ) {
	
	    _streamVolume = 100;
	    _stream_dB = 0;
	}
	else {
	
	    _streamVolume = volume;
	    
    	// volume -> dB 変換
        // 10dBで音量2倍。
        // →(求めるdB)
        //     = 10 * log2(音量)
        //     = 10 * ( log10(音量) / log10(2) )
        //     = 33.2... * log10(音量)
        _stream_dB = (long)(33.2f * log10(_streamVolume * 0.01f) * 10);
        // log10に0や負の数はダメよ〜
	}	
	    
    // ミキサーのフェーダー（volume)を設定する
    if( _axvpbStreamL != NULL ) {
        
        MIXSetFader(_axvpbStreamL, _stream_dB);
    }
    
    if( _axvpbStreamR != NULL ) {
        
        MIXSetFader(_axvpbStreamR, _stream_dB);
    }
}

s32 YSound_GetStreamVolume(void) 
{
    return _streamVolume;
}

void YSound_StopStream(void)
{
    if( _streamState != YSOUND_STREAM_STATE_STARTED &&
    	_streamState != YSOUND_STREAM_STATE_PAUSE ) {
        
        return;
    }

    _streamState = YSOUND_STREAM_STATE_STOPPING;
}

void YSound_PauseStream(void) 
{
    BOOL old = OSDisableInterrupts();

    if( _streamState != YSOUND_STREAM_STATE_STARTED ) {
        
        return;
    }

    AXSetVoiceState(_axvpbStreamL, AX_PB_STATE_STOP);
    AXSetVoiceState(_axvpbStreamR, AX_PB_STATE_STOP);
        
    OSRestoreInterrupts(old);
       
    _streamState = YSOUND_STREAM_STATE_PAUSE;    
}

void YSound_ResumeStream(void) 
{
    BOOL old = OSDisableInterrupts();

    if( _streamState != YSOUND_STREAM_STATE_PAUSE ) {
        
        return;
    }

    AXSetVoiceState(_axvpbStreamL, AX_PB_STATE_RUN);
    AXSetVoiceState(_axvpbStreamR, AX_PB_STATE_RUN);
        
    OSRestoreInterrupts(old);
    
    _streamState = YSOUND_STREAM_STATE_STARTED;
}

void YSound_LoadOnMemoryFile(const char* fileNameTable, const char* fileNameData)
{
    // サウンドテーブルの読み込み
    _spTable = (SPSoundTable*)_loadFileIntoMemory(fileNameTable);
    ASSERT(_spTable != NULL);

    // サウンドデータの読み込み
    _spData  = (u8*)_loadFileIntoMemory(fileNameData);
    ASSERT(_spData != NULL);

    // サウンドテーブルの初期化
    SPInitSoundTable(_spTable, _spData, NULL);
    
    // OnMemory情報管理リストの初期化
    _initOnMemoryInfo();
}

s32 YSound_Play(u32 index)
{
    BOOL old = OSDisableInterrupts();

    // 空管理データの取得
    int onMemoryInfoIndex = _getOnMemoryInfoIndex();
    
    if( onMemoryInfoIndex >= 0 ) {
            
        
        OnMemoryInfo* omi = &_onMemoryInfo[onMemoryInfoIndex];
                            
        // voiceの取得            
        omi->axvpb = AXAcquireVoice(15, _callbackDropVoice, 0);
        
        if( omi->axvpb != NULL )
        {
            // voiceが取得できた
            
            // サウンドテーブルからindex番目の情報を取得する
            omi->spSoundEntry = SPGetSoundEntry(_spTable, index);

            // voiceをサウンドテーブルから取得した情報で初期化する
            SPPrepareSound(omi->spSoundEntry, omi->axvpb, omi->spSoundEntry->sampleRate);

            // ミキサーの初期化
            MIXInitChannel(omi->axvpb, 0, 0, -960, -960, -960, 64, 127, 0);
            
            // voiceの再生開始
            AXSetVoiceState(omi->axvpb, AX_PB_STATE_RUN);
        }
    }

    OSRestoreInterrupts(old);
    
    return onMemoryInfoIndex;
}


//**************************************************************
//-----------------------------------------------------------
//スピーカー再生用サウンドリクエスト
//こいつを追加！
//  index  SoundIndex
//-----------------------------------------------------------
//**************************************************************
s32 YSound_PlaySpeaker(u32 index)
{
    BOOL old = OSDisableInterrupts();

    // 空管理データの取得
    int onMemoryInfoIndex = _getOnMemoryInfoIndex();
    
    if( onMemoryInfoIndex >= 0 ) {
            
        
        OnMemoryInfo* omi = &_onMemoryInfo[onMemoryInfoIndex];
                            
        // voiceの取得            
        omi->axvpb = AXAcquireVoice(15, _callbackDropVoice, 0);
        
        if( omi->axvpb != NULL )
        {
            // サウンドテーブルからindex番目の情報を取得する
            omi->spSoundEntry = SPGetSoundEntry(_spTable, index);

            // voiceをサウンドテーブルから取得した情報で初期化する
            SPPrepareSound(omi->spSoundEntry, omi->axvpb, omi->spSoundEntry->sampleRate);

            // ミキサーの初期化
            MIXInitChannel(omi->axvpb, 0, 0, -960, -960, -960, 64, 127, 0);
            
			//********************************************************************
            // リモコンボリュームセット ラメータは適当
            MIXRmtSetVolumes(omi->axvpb, 1 
            , 0, 0, -960, -960, -960, -960, -960, -960);
     
        	//スピーカで再生を行うよう設定
            AXSetVoiceRmtOn(omi->axvpb, AX_PB_REMOTE_ON);
			//********************************************************************
            
            AXSetVoiceState(omi->axvpb, AX_PB_STATE_RUN);

            OSReport("再生実行しますた。\n");
            
        }
    }

    OSRestoreInterrupts(old);
    
    return onMemoryInfoIndex;
}







void YSound_StopAll(void)
{
    BOOL old = OSDisableInterrupts();
	
	int i;
	
    for( i = 0; i < ONMEMORY_MAX_VOICES; i++ ) {
    
        if( _onMemoryInfo[i].axvpb != NULL ) {
        
            AXSetVoiceState(_onMemoryInfo[i].axvpb, AX_PB_STATE_STOP);
        }
    }

    OSRestoreInterrupts(old);
}

void YSound_Stop(s32 onMemoryInfoIndex)
{
    BOOL old = OSDisableInterrupts();
   
    if( _onMemoryInfo[onMemoryInfoIndex].axvpb != NULL ) {
        
        AXSetVoiceState(_onMemoryInfo[onMemoryInfoIndex].axvpb, AX_PB_STATE_STOP);
    }

    OSRestoreInterrupts(old);
}

void YSound_StopLooping(s32 onMemoryInfoIndex)
{
    BOOL old = OSDisableInterrupts();

    if( ( _onMemoryInfo[onMemoryInfoIndex].axvpb != NULL ) && _isLooped(_onMemoryInfo[onMemoryInfoIndex].spSoundEntry) )
    {
        SPPrepareEnd(_onMemoryInfo[onMemoryInfoIndex].spSoundEntry, _onMemoryInfo[onMemoryInfoIndex].axvpb);
    }

    OSRestoreInterrupts(old);
}

void YSound_SetOnMemoryVolume(s32 volume) 
{
	int i;
	
    // volumeは0〜100の間
	if( volume <= 0 ) {
	
	    _onMemoryVolume = 0;
	    _onMemory_dB = -960;
	}
	else if( volume >= 100 ) {
	
	    _onMemoryVolume = 100;
	    _onMemory_dB = 0;
	}
	else {
	
	    _onMemoryVolume = volume;
	    
    	// volume -> dB 変換
        // 10dBで音量2倍。
        // →(求めるdB)
        //     = 10 * log2(音量)
        //     = 10 * ( log10(音量) / log10(2) )
        //     = 33.2... * log10(音量)
        _onMemory_dB = (long)(33.2f * log10(_onMemoryVolume * 0.01f) * 10);
        // log10に0や負の数はダメよ〜
	}	
	    
    // ミキサーのフェーダー（volume)を設定する
    for( i = 0; i < ONMEMORY_MAX_VOICES; i++ ) {
    
        if( _onMemoryInfo[i].axvpb != NULL ) {
        
            MIXSetFader(_onMemoryInfo[i].axvpb, _onMemory_dB);
        }
    }
}

s32 YSound_GetOnMemoryVolume(void)
{
    return _onMemoryVolume;
}

