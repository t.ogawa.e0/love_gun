//  ヘッダファイルへ出力されるコメントをここに書く
//  ********************
//  WINDSWEEP
//  ********************
#define WINDSWEEP		0
//  ********************
//  ONE SHOT
//  ********************
#define ONE_SHOT		1
//  ********************
//  LOOP SHOT
//  ********************
#define LOOP_SHOT		2
//  ********************
//  HELI LOOP
//  ********************
#define HELI_LOOP		3
//  ********************
//  EXPLOSION
//  ********************
#define EXPLOSION		4
//  ********************
//  EXPLOSION LOOP
//  ********************
#define EXPLOSION_LOOP		5
//  *******************
//  MOTORCYCLE
//  *******************
#define MOTORCYCLE_LOOP		6
//  *******************
//  MENU CURSOR
//  *******************
#define MENU_CURSOR		7
//  *******************
//  MENU SELECT
//  *******************
#define MENU_SELECT		8
