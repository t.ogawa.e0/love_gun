#ifndef _ENEMY_H_
#define _ENEMY_H_

#define MAX_ENEMY (20)

void EnemyInit(GXTexObj* pTex, int nCount, int ID, int TexNunBer);
void EnemyUninit(void);
void EnemyUpdate(void);
void EnemyDraw(GXTexObj* pTex, int nCount);
void EnemyMove(void);

int EnemyGetTexNunBer(int NunBer);

#endif // !_ENEMY_H_
