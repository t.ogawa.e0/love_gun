#ifndef _MAIN_H_
#define _MAIN_H_

#include "DEBUG____.h"

//------------------------------------------------------------------------
//	�萔�}�N����`
//#define SCREEN_WIDTH (640)	//���𑜓x	4:3
#define SCREEN_WIDTH (854)	//���𑜓x	16:9
#define SCREEN_HEIGHT (480)	//�c�𑜓x	4:3

#define SCREENROOC (0)

#define bool BOOL
#define false FALSE
#define true TRUE
#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif
#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif
#ifndef _BOOL_
#define _BOOL_(a) ((a) ^ 1)
#endif // !_BOOL_

void Init(void);
void Uninit(void);
void Update(void);
void Draw(void);

#endif