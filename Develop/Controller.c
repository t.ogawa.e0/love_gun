#include <revolution.h>
#include <revolution/kpad.h>
#define DEMO_USE_MEMLIB
#include <demo.h>			//
#include <revolution/gx.h> 	// OSFatalを使用する場合includeが必要

#include "mem2allocator.h"
#include <math.h>
#include <stdio.h>			// sprintfなども使用できます
#include "main.h"
#include "Controller.h"


static KPADStatus kpadStatus;
static float px, py;

void ControllerInit(void)
{
	KPADInit();
}
void ControllerUninit(void)
{
	
}
void ControllerUpdate(void)
{
	f32 rot;
	f32 Oldrot;
	f32 Distance;
	f32 OldDistance;

	//コントラー情報を取得
	KPADRead(WPAD_CHAN0, &kpadStatus, 1);

	px = kpadStatus.pos.x;
	py = kpadStatus.pos.y;

	px = px*(SCREEN_WIDTH / 2) + (SCREEN_WIDTH / 2);
	py = py*(SCREEN_HEIGHT / 2) + (SCREEN_HEIGHT / 2);

	//傾き取得
	rot = 0;
	rot = atan2f(kpadStatus.horizon.y, kpadStatus.horizon.x);
	kpadStatus.horizon.y = 0.0f; kpadStatus.horizon.x = 0.0f;

	//センサーバーとの距離取得
	Distance = 0;
	Distance = kpadStatus.dist;
	if (!(OldDistance == Distance))
	{
		OldDistance = Distance;
	}
}
void ControllerDraw(void)
{

}

float GetPX(void)
{
	return px;
}
float GetPY(void)
{
	return py;
}

int FadeFlag(void)
{
	
	if (kpadStatus.trig&KPAD_BUTTON_A)
	{
		return true;
	}
	
	return false;
}

int KeyB(void)
{
	if (kpadStatus.trig&KPAD_BUTTON_B)
	{
		return true;
	}
	
	return false;
}