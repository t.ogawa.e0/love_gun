#ifndef _HOME_H_
#define _HOME_H_

typedef enum
{
	TEXTURE_NUM_HOME = 0,
	TEXTURE_FontMan,
	TEXTURE_FontGale,
	TEXTURE_HOMEMan,
	TEXTURE_HOMEGale,
	TEXTURE_HOME_MAX
}TEXTURE_HOME_DATA;

void HomeCase(void);
void HomeInit(void);
void HomeInit2(void);
void HomeUninit(void);
void HomeUpdate(void);
void HomeDraw(void);
void Home1(void);
void HomeMan(void);
void HomeGale(void);
int HomeGetFade(void);
int GetPlayerID(void);

#endif // !_HOME_H_
