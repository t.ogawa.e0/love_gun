#ifndef _BULLET_H_
#define _BULLET_H_

void BulletInit(void);
void BulletUninit(void);
void BulletUpdate(void);
void BulletDraw(GXTexObj* pTex);

#endif // !_BULLET_H_
