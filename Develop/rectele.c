#include <revolution.h>
#include <revolution/kpad.h>
#define DEMO_USE_MEMLIB
#include <demo.h>			//
#include <revolution/gx.h> 	// OSFatalを使用する場合includeが必要

#include "mem2allocator.h"

#include <math.h>
#include <stdio.h>			// sprintfなども使用できます
#include "main.h"
#include "sprite.h"
#include "Controller.h"
#include "rectele.h"

static TPLPalettePtr texPal;				//テクスチャパレットを表示する型の変数
static GXTexObj RecteleTexObj[TEXTURE_RECTELE_MAX];	//テクスチャオブジェクトを保存する配列

void RecteleCase(void)
{
	RecteleUpdate();
	RecteleDraw();
}
void RecteleInit(void)
{
	int nCount;

	// TPLファイルノロード
	TPLGetPalette(&texPal, "cursor.tpl");

	//ロードエラーチェック
	ASSERTMSG(TEXTURE_RECTELE_MAX == texPal->numDescriptors, "\nTPL内画像総数とNUM_MAXの値が一致していない\n(numDescriptors==%d)!=(NUM_MAX=%d)\n", texPal->numDescriptors, TEXTURE_RECTELE_MAX);

	//TPLからテクスチャオブジェクトを取り出す
	for (nCount = 0; nCount < texPal->numDescriptors; nCount++)
	{
		TPLGetGXTexObjFromPalette(texPal, &RecteleTexObj[nCount], nCount);
	}
}
void RecteleUninit(void)
{

}
void RecteleUpdate(void)
{

}
void RecteleDraw(void)
{
	// レクテル
	SpriteSetup();
	SpriteSetColor(255, 255, 255, 255);
	SpritrSetScale(1.0f, 1.0f);
	TexSetSize(0, 0, 42, 42);
	SpritrSetSize(GetPX(), GetPY(), 42, 42);
	SpriteDraw(&RecteleTexObj[TEXTURE_NUM_RECTELE]);
}