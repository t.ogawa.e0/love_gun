#ifndef _NUMBER_H_
#define _NUMBER_H_

#include <revolution.h>
#include <revolution/kpad.h>
#define DEMO_USE_MEMLIB
#include <demo.h>			//
#include <revolution/gx.h> 	// OSFatalを使用する場合includeが必要

#define NUMBER_TOTAL_W (800) // ナンバーリストの幅
#define NUMBER_TOTAL_H (165) // ナンバーリストの高さ
#define NUM_NUMBER (10) // 数字の数
#define NUM_NUMBER_W (NUM_NUMBER) // 横の数字の数
#define NUMBER_W (NUMBER_TOTAL_W / NUM_NUMBER_W) // 数字1つの幅
#define NUMBER_H (NUMBER_TOTAL_H) // 数字1つの高さ

//=================================================================
// マクロ定義
#define PUSYU_SCORE (10) // 繰り上げ

int NumberInit(int Digit);
void NumberUninit(void);
void NumberUpdate(void);
void NumberDraw(int Digit, int Max, bool Zero, bool LeftAlignment, int Score);

void NumberSpriteSetColor(u8 r, u8 g, u8 b, u8 a);
void NumberSpritrSetScale(float ScaleW, float ScaleH);
void NumberTexSetSize(int tx, int ty, int tw, int th);
void NumberSpritrSetSize(float dx, float dy, float dw, float dh);
void NumberSetAnimasion(int Count, int Flame, int Pattan, int Direction);
void NumberSetDraw(GXTexObj* pTex);

#endif // !_NUMBER_H_
