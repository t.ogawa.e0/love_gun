**Love Gun**
====

**Overview**
====

*2017/9月に制作した、[Wii](https://www.nintendo.co.jp/wii)で動くゲームです。<br>*
*コンパイルには専用の開発機 [NINTENDO Wii開発機NDEV(Wireless) RVT-001]() が必要です。<br>*
*開発に利用した統合開発環境は[CodeWarrior](https://ja.wikipedia.org/wiki/CodeWarrior)です。<br>*
*使用言語は[C言語](https://ja.wikipedia.org/wiki/C%E8%A8%80%E8%AA%9E)です。<br>*
*  [Develop](Develop) - Love Gun の開発データです。

**Requirement**
====

![c](Asset/c.png)![wii](Asset/wii.png)